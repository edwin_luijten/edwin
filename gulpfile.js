var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
		'../assets/css/zero.css'
	], 'public/assets/css/app.css');

	mix.scripts([
		'../assets/js/lib.js',
		'../assets/js/tracker.global.js',
		'../assets/js/tracker.js'
	], 'public/assets/js/app.js');

	mix.version(['assets/css/app.css', 'assets/js/app.js']);
});
