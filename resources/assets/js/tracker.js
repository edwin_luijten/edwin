$.pjax.defaults.timeout = 1200;
(function() {
	window.GyroscopeZero = (function() {
		function GyroscopeZero() {
			this.home = new ZeroHome();
			this.explorer = new ZeroExplorer();
			this.sport = new ZeroSport();
			this.digital = new ZeroDigital();
			this.details = new ZeroDetails();
		}

		GyroscopeZero.prototype.init = function(section, page) {

			L.mapbox.accessToken = 'pk.eyJ1IjoiZWR3aW5sdWlqdGVuIiwiYSI6Im42QXRfRzAifQ._XjAbHNLqs7Fpb-G_5UfsQ';

			if (section === 'home') {
				return this.home.init();
			} else if (section === 'explorer') {
				return this.explorer.init(page);
			} else if (section === 'sport') {
				return this.sport.init();
			} else if (section === 'digital') {
				return this.digital.init();
			} else if (section === 'about') {
				return gyroscope.extras.startIncrementingAge();
			} else if (section === 'details') {
				return this.details.init();
			}
		};

		return GyroscopeZero;

	})();

	window.ZeroDetails = (function() {
		function ZeroDetails() {}

		ZeroDetails.prototype.init = function() {

			return this.initializeMaps();
		};

		ZeroDetails.prototype.initializeMaps = function() {
			return this.initializeMap($('.motion-map'));
		};

		ZeroDetails.prototype.initializeMap = function(el) {
			var map, map_id, map_style, points, raw, satellite_style, vector_style;
			map = $(el);
			if (!map.length) {
				return;
			}
			map_id = map.attr('id');
			satellite_style = 'gyroscope.kkb54012';
			vector_style = 'gyroscope.kkbbbn8d';
			vector_style = 'gyroscope.klopjd44';
			vector_style = 'gyroscope.k8ga08k9';
			map_style = vector_style;
			raw = map.find('.raw-points').html();

			if (!raw) {
				return;
			}
			points = eval(raw);
			if (points) {
				return this.createMapWithPoints(map_style, points, map_id, '#30d165', 10);
			}
		};

		ZeroDetails.prototype.createMapWithPoints = function(map_style, points, map_id, line_color, bottom_padding) {
			var bounds, bounds_options, line, line_options, map, map_options, total_length;
			bottom_padding = bottom_padding || 0;
			line_color = line_color || '#444';
			line_options = {
				color: line_color,
				smoothFactor: 1.5,
				weight: 3,
				opacity: 1,
				fillOpacity: 0.8,
				lineCap: 'round'
			};
			map_options = {
				keyboard: false,
				dragging: false,
				touchZoom: false,
				scrollWheelZoom: false,
				doubleClickZoom: false,
				boxZoom: false,
				tap: false,
				zoomControl: false,
				attributionControl: false
			};
			line = L.polyline(points, line_options);
			bounds = line.getBounds();
			bounds_options = {
				paddingTopLeft: [100, 80],
				paddingBottomRight: [100, 80]
			};
			map = L.mapbox.map(map_id, map_style, map_options).fitBounds(bounds, bounds_options);
			line.addTo(map);
			total_length = line._path.getTotalLength();
			$(line._path).css({
				'stroke-dasharray': total_length + 10,
				'stroke-dashoffset': total_length + 10
			});
			timeoutSet(50, function() {
				return $('#' + map_id).addClass('showing');
			});
			return map;
		};

		return ZeroDetails;

	})();

	window.ZeroDigital = (function() {
		function ZeroDigital() {
			this.bindInfiniteScroll();
		}

		ZeroDigital.prototype.init = function() {
			timeoutSet(580, (function(_this) {
				return function() {
					return _this.hydratePhotos();
				};
			})(this));
			timeoutSet(800, (function(_this) {
				return function() {
					return _this.loadTwitterJs();
				};
			})(this));
			return timeoutSet(1000, (function(_this) {
				return function() {
					return _this.startInfiniteScroll();
				};
			})(this));
		};

		ZeroDigital.prototype.hydratePhotos = function() {
			return $('#page .dehydrated').each((function(_this) {
				return function(n, el) {
					var photo, src;
					photo = $(el);
					src = photo.attr('data-src');
					photo.attr('src', src);
					return photo.removeClass('dehydrated');
				};
			})(this));
		};

		ZeroDigital.prototype.loadTwitterJs = function() {
			return $('#page').append('<script async src="//platform.twitter.com/widgets.js" charset="utf-8" />');
		};

		ZeroDigital.prototype.startInfiniteScroll = function() {
			this.page = $('#page .photos-content');
			this.pageHeight = this.page.height();
			this.finished = false;
			return this.loading = false;
		};

		ZeroDigital.prototype.bindInfiniteScroll = function() {
			var scroll_threshold;
			scroll_threshold = 1600;
			return $(window).scroll($.throttle(150, (function(_this) {
				return function(e) {
					var position;
					if (_this.finished || _this.loading) {
						return;
					}
					position = $(window).scrollTop();
					if (position > _this.pageHeight - scroll_threshold) {
						return _this.loadMoreContent();
					}
				};
			})(this)));
		};

		ZeroDigital.prototype.loadMoreContent = function() {
			var url;
			if (this.loading) {
				return;
			}
			if (this.finished) {
				return;
			}
			url = $('#page .photos-content .more-content').last().attr('data-url');
			if (url) {
				this.loading = true;
				return this.newContentLoading = $.ajax({
					method: 'get',
					dataType: 'html',
					url: url,
					success: (function(_this) {
						return function(data) {
							return _this.appendUserData(data);
						};
					})(this),
					error: (function(_this) {
						return function() {
							return _this.loading = false;
						};
					})(this)
				});
			}
		};

		ZeroDigital.prototype.appendUserData = function(data) {
			var bunch, container;
			bunch = $(data).find('.photo-bunch');
			container = $('#page .photos-content');
			container.append(bunch);
			this.pageHeight = this.page.height();
			this.loading = false;
			if ($('.no.more-conte nt', container).length) {
				this.finished = true;
				return $('body').addClass('finished-scrolling');
			}
		};

		return ZeroDigital;

	})();

	window.ZeroExplorer = (function() {
		function ZeroExplorer() {
			this.bindHexes();
			this.bindDays();
			this.bindVisits();
			this.bindKeystrokes();
		}

		ZeroExplorer.prototype.init = function(page) {

			if (page === 'months') {
				this.incrementSeconds();
				return this.plotPoint($('#page .map-container .blip'));
			} else if (page === 'month') {
				this.scrollLock = false;
				this.preformatDays();
				this.revealDays();
				this.delayHexes();
				this.setupScrollWatch();
				return timeoutSet(1800, function() {
					return $('.month-days').addClass('scrollable');
				});
			}
		};

		ZeroExplorer.prototype.incrementSeconds = function() {
			var el, increment_interval;
			el = $('.incrementable-seconds');
			this.seconds = this.seconds || parseInt(el.data('seconds'));
			increment_interval = intervalSet(40, (function(_this) {
				return function() {
					el.text(numberWithCommas(_this.seconds));
					return _this.seconds = (parseFloat(_this.seconds) + 0.04).toFixed(2);
				};
			})(this));
			return gyroscope.aileron.intervals.push(increment_interval);
		};

		ZeroExplorer.prototype.plotPoint = function(el, size) {
			var coords, latitude, longitude;
			size = size || 'large';
			latitude = el.data('latitude');
			longitude = el.data('longitude');
			coords = this.getMapProjection(size)([longitude, latitude]);
			el.css({
				left: parseInt(coords[0]),
				top: parseInt(coords[1])
			}).addClass('positioned');
			return timeoutSet(450, function() {
				return el.addClass('showing');
			});
		};

		ZeroExplorer.prototype.getMapProjection = function(size) {
			var height, projection, translation, width;
			if (size === 'small') {
				width = 240;
				height = 160;
				translation = 1.4;
			} else {
				width = 700;
				height = 320;
				translation = 1;
			}
			return projection = d3.geo.mercator().scale((width + 1) / 2 / Math.PI).translate([width / 2, translation * height / 2]).precision(.1);
		};

		ZeroExplorer.prototype.revealDays = function() {
			var days, delay;
			if ($(window).height() < 750) {
				delay = 600;
			} else {
				delay = 0;
			}
			days = $('#page .month-days .day');
			days.each(function(n, el) {
				var day;
				day = $(el);
				n = Math.min(n, 8);
				timeoutSet(delay + 560 + 110 * n, function() {
					return day.addClass('showing-number');
				});
				return timeoutSet(delay + 900 + 220 * n, function() {
					return day.addClass('showing');
				});
			});
			return timeoutSet(delay + 1600, (function(_this) {
				return function() {
					return _this.loadPhotos(days);
				};
			})(this));
		};

		ZeroExplorer.prototype.loadPhotos = function(days) {
			return $('.dehydrated.image').each(function(n, el) {
				var photo, src;
				photo = $(el);
				src = photo.attr('data-photo');
				if (src) {
					return photo.css('background-image', 'url(' + src + ')').removeClass('dehydrated').addClass('hydrated');
				}
			});
		};

		ZeroExplorer.prototype.delayHexes = function() {
			var honeycomb;
			honeycomb = $('#page .honeycomb');
			$('.jitter', honeycomb).each(function(n, el) {
				return $(el).attr('style', '-webkit-animation-delay:' + parseInt(Math.random() * 4000) + 'ms;');
			});
			return timeoutSet(1700, function() {
				return $('body').addClass('honeycomb-activated');
			});
		};

		ZeroExplorer.prototype.pauseHexes = function() {
			return $('body').addClass('pause-hexes');
		};

		ZeroExplorer.prototype.resumeHexes = function() {
			return $('body').removeClass('pause-hexes');
		};

		ZeroExplorer.prototype.bindHexes = function() {
			$(window).on('blur', (function(_this) {
				return function(e) {
					return _this.pauseHexes();
				};
			})(this));
			$(window).on('focus', (function(_this) {
				return function(e) {
					return _this.resumeHexes();
				};
			})(this));
			$(document).on('click', '.honeycomb a.hex', (function(_this) {
				return function(e) {
					var day, days, link, scrollTime, scroll_delay, scrolledDistance, scrolling_down, x;
					e.preventDefault();
					_this.pauseHexes();
					link = $(e.currentTarget);
					if ($(window).height() < 1100) {
						days = _this.scrollDaysIntoView();
						scrolling_down = true;
					} else {
						days = $('.month-days');
					}
					day = $(link.attr('href'), days);
					$('.day', days).not(day).removeClass('highlighting showing-names');
					x = day.offset().left;
					scrolledDistance = days.scrollLeft();
					x = parseInt(x + scrolledDistance);
					if ($(window).width() > 900) {
						x = x - 500;
					} else {
						x = x - 90;
					}
					scrollTime = parseInt(400 + 0.1 * scrolledDistance);
					_this.scrollLock = true;
					if (_this.highlightDayTimeout) {
						clearTimeout(_this.highlightDayTimeout);
					}
					scroll_delay = scrolling_down ? 400 : 10;
					return _this.highlightDayTimeout = timeoutSet(scroll_delay, function() {
						$(days).stop().animate({
							scrollLeft: x
						}, scrollTime);
						timeoutSet(scrollTime + 50, function() {
							return _this.highlightDay(day);
						});
						return timeoutSet(scrollTime + 450, function() {
							return _this.scrollLock = false;
						});
					});
				};
			})(this));
			return $(document).on('mouseenter', '.honeycomb a.hex', (function(_this) {
				return function(e) {
					var hex, photo, src;
					hex = $(e.currentTarget);
					if (hex.hasClass('hydrated')) {
						return;
					}
					photo = $('.venue-photo', hex);
					src = photo.attr('data-photo');
					photo.css('background-image', 'url(' + src + ')');
					return hex.addClass('hydrated');
				};
			})(this));
		};

		ZeroExplorer.prototype.bindDays = function() {
			$(document).on('mouseenter', '.month-days .day', (function(_this) {
				return function(e) {
					var day;
					if (gyroscope.aileron.isMobile) {
						return;
					}
					if (_this.scrollLock) {
						return;
					}
					if (_this.showDayTimeout) {
						clearTimeout(_this.showDayTimeout);
					}
					day = $(e.currentTarget);
					if (!day.hasClass('showing')) {
						return;
					}
					return _this.showDayTimeout = timeoutSet(90, function() {
						return day.addClass('showing-names');
					});
				};
			})(this));
			$(document).on('mouseleave', '.month-days .day', (function(_this) {
				return function(e) {
					var day;
					if (gyroscope.aileron.isMobile) {
						return;
					}
					if (_this.scrollLock) {
						return;
					}
					if (_this.showDayTimeout) {
						clearTimeout(_this.showDayTimeout);
					}
					day = $(e.currentTarget);
					if (!day.hasClass('highlighting')) {
						return day.removeClass('showing-names');
					}
				};
			})(this));
			return $(document).on('click', '.month-days .day', (function(_this) {
				return function(e) {
					var day;
					if (gyroscope.aileron.isMobile) {
						return;
					}
					if (_this.scrollLock) {
						return;
					}
					day = $(e.currentTarget);
					if (day.hasClass('highlighting')) {
						return _this.hideDay(day);
					} else {
						return _this.revealDay(day);
					}
				};
			})(this));
		};

		ZeroExplorer.prototype.bindVisits = function() {
			var venue_trigger;
			venue_trigger = 'click';
			$(document).on('mouseenter', '.day .hoverable.visit', (function(_this) {
				return function(e) {
					var day, visit;
					e.preventDefault();
					visit = $(e.currentTarget);
					day = visit.parents('.day');
					_this.prepVenues(day);
					_this.venueClickLock = true;
					if (day.hasClass('highlighting')) {
						e.stopPropagation();
						_this.viewVisit(visit, day);
						return timeoutSet(350, function() {
							return _this.venueClickLock = false;
						});
					} else {
						return _this.venueClickLock = false;
					}
				};
			})(this));
			$(document).on('click', '.hoverable.visit', (function(_this) {
				return function(e) {
					var day, visit;
					visit = $(e.currentTarget);
					day = visit.parents('.day');
					if (day.hasClass('highlighting')) {
						e.preventDefault();
						e.stopPropagation();
						if (visit.hasClass('viewing')) {
							if (_this.venueClickLock) {
								return _this.venueClickLock = false;
							} else {
								return _this.unviewVisit(visit);
							}
						} else {
							return _this.viewVisit(visit, day);
						}
					}
				};
			})(this));
			return $(document).on('mouseleave', '.hoverable.visit', (function(_this) {
				return function(e) {
					var visit;
					visit = $(e.currentTarget);
					if (_this.showVenueTimeout) {
						clearTimeout(_this.showVenueTimeout);
					}
					_this.venueClickLock = false;
					if (visit.hasClass('viewing')) {
						return _this.unviewVisit(visit);
					}
				};
			})(this));
		};

		ZeroExplorer.prototype.viewVisit = function(visit, day) {
			var keep_details_hidden, return_grace_period, venue_open_delay;
			keep_details_hidden = 220;
			venue_open_delay = 45;
			return_grace_period = 40;
			$('.visit.viewing', day).not(visit).removeClass('viewing');
			$('.viewing.venue-info', day).removeClass('viewing');
			if (this.hideVenueTimeout) {
				clearTimeout(this.hideVenueTimeout);
			}
			if (this.showVenueTimeout) {
				clearTimeout(this.showVenueTimeout);
			}
			if (this.revealDetailsTimeout) {
				clearTimeout(this.revealDetailsTimeout);
			}
			return this.showVenueTimeout = timeoutSet(venue_open_delay, (function(_this) {
				return function() {
					return _this.openVisit(visit, day);
				};
			})(this));
		};

		ZeroExplorer.prototype.openVisit = function(visit, day) {
			var venue;
			visit.addClass('viewing');
			venue = visit.attr('data-venue');
			this.activateVenuePhotos(venue, day);
			if (visit.hasClass('viewing')) {
				return day.addClass('showing-venue');
			}
		};

		ZeroExplorer.prototype.closeVisit = function(visit) {
			var day, keep_details_hidden;
			keep_details_hidden = 220;
			visit.removeClass('viewing');
			day = visit.parents('.day');
			$('.viewing.venue-info', day).removeClass('viewing');
			return this.revealDetailsTimeout = timeoutSet(keep_details_hidden, (function(_this) {
				return function() {
					return day.removeClass('showing-venue');
				};
			})(this));
		};

		ZeroExplorer.prototype.unviewVisit = function(visit) {
			var return_grace_period;
			return_grace_period = 50;
			this.venueClickLock = false;
			if (this.showVenueTimeout) {
				clearTimeout(this.showVenueTimeout);
			}
			if (this.hideVenueTimeout) {
				clearTimeout(this.hideVenueTimeout);
			}
			if (this.revealDetailsTimeout) {
				clearTimeout(this.revealDetailsTimeout);
			}
			return this.hideVenueTimeout = timeoutSet(return_grace_period, (function(_this) {
				return function() {
					return _this.closeVisit(visit);
				};
			})(this));
		};

		ZeroExplorer.prototype.activateVenuePhotos = function(venue, day) {
			var venue_info;
			if (!$('#device-info').hasClass('desktop')) {
				return;
			}
			venue_info = day.find('.venues .' + venue);
			if (venue_info.length) {
				$('.viewing.venue-info', day).not(venue_info).removeClass('viewing');
				return venue_info.addClass('viewing');
			}
		};

		ZeroExplorer.prototype.areDaysOpen = function() {
			return $('.day.highlighting').length;
		};

		ZeroExplorer.prototype.hideDayIfOpen = function() {
			return this.hideDay($('.day.highlighting'));
		};

		ZeroExplorer.prototype.hideDay = function(day) {
			if (this.showVenueTimeout) {
				clearTimeout(this.showVenueTimeout);
			}
			day.removeClass('highlighting highlighting-venues showing-venue');
			$('.visit.viewing', day).removeClass('viewing');
			return this.resumeHexes();
		};

		ZeroExplorer.prototype.highlightDay = function(day) {
			this.pauseHexes();
			day.addClass('highlighting showing-names');
			return timeoutSet(1000, (function(_this) {
				return function() {
					return _this.prepVenues(day);
				};
			})(this));
		};

		ZeroExplorer.prototype.prepVenues = function(day) {
			var photos;
			photos = $('.venue-photo.dehydrated', day);
			return photos.each(function(n, el) {
				var photo, src;
				photo = $(el);
				src = photo.attr('data-photo');
				return photo.css('background-image', 'url(' + src + ')').removeClass('dehydrated');
			});
		};

		ZeroExplorer.prototype.scrollToAndHighlight = function(day, days) {
			var left_clearance, min_left_clearance, min_right_clearance, right_clearance, scrollTime, scrolled, window_width, x;
			window_width = $(window).width();
			left_clearance = day.offset().left;
			if (window_width > 1400) {
				min_left_clearance = 450;
			} else {
				min_left_clearance = 100;
			}
			right_clearance = window_width - left_clearance;
			min_right_clearance = 650;
			if (right_clearance < min_right_clearance) {
				this.scrollLock = true;
				scrolled = days.scrollLeft();
				x = scrolled + (min_right_clearance - right_clearance);
				scrollTime = parseInt(Math.min(600, 1.5 * Math.abs(x - scrolled) + 50));
				timeoutSet(scrollTime + 20, (function(_this) {
					return function() {
						return _this.highlightDay(day);
					};
				})(this));
				days.stop().animate({
					scrollLeft: x
				}, scrollTime, 'easeInOutQuad');
				return timeoutSet(scrollTime + 350, (function(_this) {
					return function() {
						return _this.scrollLock = false;
					};
				})(this));
			} else if (left_clearance < min_left_clearance) {
				this.scrollLock = true;
				scrolled = days.scrollLeft();
				x = scrolled - (min_left_clearance - left_clearance);
				scrollTime = parseInt(Math.min(600, 1.5 * Math.abs(x - scrolled) + 50));
				timeoutSet(scrollTime + 20, (function(_this) {
					return function() {
						return _this.highlightDay(day);
					};
				})(this));
				days.stop().animate({
					scrollLeft: x
				}, scrollTime, 'easeInOutQuad');
				return timeoutSet(scrollTime + 350, (function(_this) {
					return function() {
						return _this.scrollLock = false;
					};
				})(this));
			} else {
				this.highlightDay(day);
				return timeoutSet(320, (function(_this) {
					return function() {
						return _this.scrollLock = false;
					};
				})(this));
			}
		};

		ZeroExplorer.prototype.revealDay = function(day, delay) {
			var days;
			if (delay == null) {
				delay = 0;
			}
			days = this.scrollDaysIntoView();
			$('.day', days).not(day).removeClass('highlighting highlighting-venues showing-names');
			if (delay) {
				this.scrollLock = true;
				return timeoutSet(delay, (function(_this) {
					return function() {
						return _this.scrollToAndHighlight(day, days);
					};
				})(this));
			} else {
				return this.scrollToAndHighlight(day, days);
			}
		};

		ZeroExplorer.prototype.scrollDaysIntoView = function() {
			var days, y;
			days = $('#page .month-days').addClass('scrollable');
			y = days.offset().top;
			y = y + $('body').scrollTop();
			$('body').stop().animate({
				scrollTop: y
			}, 500);
			return days;
		};

		ZeroExplorer.prototype.setupScrollWatch = function() {
			var days;
			days = $('#page .month-days');
			this.lastScrollPosition = 0;
			return days.scroll($.throttle(220, (function(_this) {
				return function(e) {
					var newScrollPosition;
					newScrollPosition = days.scrollLeft();
					if (Math.abs(_this.lastScrollPosition - newScrollPosition) > 30) {
						if (!_this.scrollLock) {
							$('.day.highlighting').removeClass('highlighting highlighting-venues showing-names');
						}
					}
					return _this.lastScrollPosition = newScrollPosition;
				};
			})(this)));
		};

		ZeroExplorer.prototype.bindKeystrokes = function() {
			return $(window).on('keydown', (function(_this) {
				return function(e) {
					if (!(gyroscope.aileron.currentSection === 'explorer' && gyroscope.aileron.currentPage === 'month')) {
						return;
					}
					if (e.which === 37) {
						if (_this.areDaysOpen()) {
							e.preventDefault();
							if (_this.scrollLock) {
								return;
							}
							return _this.jumpWhileOpened(-1);
						}
					} else if (e.which === 39) {
						if (_this.areDaysOpen()) {
							e.preventDefault();
							if (_this.scrollLock) {
								return;
							}
							return _this.jumpWhileOpened(1);
						}
					} else if (e.which === 27) {
						return _this.hideDayIfOpen();
					}
				};
			})(this));
		};

		ZeroExplorer.prototype.jumpWhileOpened = function(amount) {
			var current, goto;
			if (this.scrollLock) {
				return;
			}
			current = $('.month-days .day.highlighting');
			if (!current.length) {
				return;
			}
			if (amount < 0) {
				goto = current.prev('.day');
			} else if (amount > 0) {
				goto = current.next('.day');
			}
			if (!goto.length) {
				return this.hideDayIfOpen();
			} else {
				return this.revealDay(goto, 90);
			}
		};

		ZeroExplorer.prototype.preformatDays = function() {
			$('#page .month-days .simple.day').each((function(_this) {
				return function(n, el) {
					return _this.preformatSimpleDayVisits(el);
				};
			})(this));
			return $('#page .month-days .day').each((function(_this) {
				return function(n, el) {
					return _this.preformatInlinePhotos(el);
				};
			})(this));
		};

		ZeroExplorer.prototype.preformatInlinePhotos = function(day) {
			var photos;
			photos = $('.inline.photos .photo', day);
			if (!photos.length) {
				return;
			}
			return this.spreadItems(photos, 'photos', day);
		};

		ZeroExplorer.prototype.preformatSimpleDayVisits = function(day) {
			var visits;
			visits = $('.visits .visit', day);
			if (!visits.length) {
				return;
			}
			return this.spreadItems(visits, 'visits', day);
		};

		ZeroExplorer.prototype.spreadItems = function(visits, type, day) {
			var depth, end_is_full, extreme_depth, extreme_overlap_threshold, overlap_threshold, prev, previous_start, total_extreme_overlapping, total_overlapping;
			end_is_full = visits.last().attr('data-start') > 90;
console.log('spread items');
			console.log(end_is_full);
			previous_start = 0;
			total_overlapping = 0;
			total_extreme_overlapping = 0;
			depth = 0;
			extreme_depth = 0;
			overlap_threshold = 15;
			extreme_overlap_threshold = 2.5;
			prev = false;
			visits.each(function(n, el) {
				var start, visit;
				visit = $(el);
				start = visit.attr('data-start');
				if (visit.hasClass('at-end')) {
					start = start - 7;
				} else if (visit.hasClass('towards-end')) {
					start = start - 4;
				}
				if (previous_start && previous_start > start - extreme_overlap_threshold) {
					if (prev) {
						prev.addClass('about-to-extremely-overlap');
					}
					visit.addClass('extremely-impacted');
					extreme_depth += 1;
					total_overlapping += 1;
					overlap_threshold += 5;
				} else {
					if (extreme_depth) {
						extreme_depth = 0;
					}
					if (previous_start && previous_start > start - overlap_threshold) {
						if (!depth && prev) {
							prev.addClass('about-to-overlap');
						}
						visit.addClass('impacted');
						total_overlapping += 1;
						overlap_threshold += 5;
						depth += 1;
					} else {
						if (depth) {
							depth = 0;
							prev.addClass('gets-ok');
						}
						if (previous_start + 5 + overlap_threshold < start) {
							visit.addClass('clear');
						}
						overlap_threshold = 10;
					}
				}
				previous_start = start;
				return prev = visit;
			});
			if (total_overlapping && type === 'visits') {
				$(day).addClass('has-overlaps');
				if (end_is_full) {
					return $(day).addClass('end-overloaded');
				}
			}
		};

		return ZeroExplorer;

	})();

	window.ZeroHome = (function() {
		function ZeroHome() {
			this.bindSpinner();
			this.bindLinks();
		}

		ZeroHome.prototype.init = function() {
			this.previewing = 'sport';
			$('body').addClass('preview-sport');
			this.plotExplorerPoint();
			return this.createRunMap();
		};

		ZeroHome.prototype.createRunMap = function() {
			var map_style, points;
			map_style = 'gyroscope.k8ga08k9';
			if ($('#device-info').hasClass('iframed')) {
				map_style = false;
			}
			points = eval($('#page .mapbox-map .raw-points').first().text());

			if (points) {
				return this.createMap(map_style, points, 'homepage-run-map', '#00aeef', 90);
			}
		};

		ZeroHome.prototype.plotExplorerPoint = function() {
			return gyroscope.zero.explorer.plotPoint($('.map-container .blip'), 'small');
		};

		ZeroHome.prototype.bindSpinner = function() {
			$(document).on('mouseenter', '.sections-nav a', (function(_this) {
				return function(e) {
					var link, section, url;
					link = $(e.currentTarget);
					url = link.attr('href');
					section = link.attr('data-section');
					if (section === _this.previewing) {
						return;
					}
					return _this.switchHomePreviewTo(section);
				};
			})(this));
			$(document).on('click', '.full-preview', (function(_this) {
				return function(e) {
					var link, section;
					e.preventDefault();
					link = $(e.currentTarget);
					section = link.data('section');
					if (section === _this.previewing) {
						if (link.parent().hasClass('locked')) {
							return;
						}
						_this.goToSection(section);
						gyroscope.aileron.readyToSwitch = false;
						return gyroscope.aileron.delayedGoTo(link.attr('href'), link.data('section'), link.data('level'));
					} else {
						return _this.switchHomePreviewTo(section);
					}
				};
			})(this));
			$(document).on('mouseenter', '.full-preview', (function(_this) {
				return function(e) {
					var link, section;
					e.preventDefault();
					link = $(e.currentTarget);
					section = link.data('section');
					if (link.parent().hasClass('locked')) {
						return;
					}
					return _this.switchHomeHoverTo(section);
				};
			})(this));
			return $(document).on('mouseleave', '.full-preview', (function(_this) {
				return function(e) {
					return _this.switchHomeHoverTo(false);
				};
			})(this));
		};

		ZeroHome.prototype.bindLinks = function() {
			$(document).on('click', '.spinner a.transitioned', (function(_this) {
				return function(e) {
					var link, section;
					link = $(e.currentTarget);
					section = link.attr('data-section');
					return _this.goToSection(section);
				};
			})(this));
			$(document).on('click', '.sections-nav a', (function(_this) {
				return function(e) {
					var link, section;
					link = $(e.currentTarget);
					if (link.hasClass('locked')) {
						return;
					}
					section = link.attr('data-section');
					return _this.goToSection(section);
				};
			})(this));
			return $(document).on('click', '.home-footer a', (function(_this) {
				return function(e) {
					return _this.goToSection(_this.previewing);
				};
			})(this));
		};

		ZeroHome.prototype.switchHomePreviewTo = function(section) {
			if (!$('body').hasClass('animating')) {
				return;
			}
			this.previewing = section;
			return $('body').removeClass('preview-sport preview-explorer preview-digital intro').removeClass('hover-sport hover-explorer hover-digital').addClass('preview-' + section);
		};

		ZeroHome.prototype.switchHomeHoverTo = function(section) {
			var body;
			body = $('body').removeClass('hover-sport hover-explorer hover-digital');
			if (section) {
				return body.addClass('hover-' + section);
			}
		};

		ZeroHome.prototype.goToSection = function(section) {
			$('#page .sections-nav .selection').addClass('to-' + section);
			return $('#page .spinner').addClass('to-' + section);
		};

		ZeroHome.prototype.getMapProjection = function() {
			var height, projection, translation, width;
			width = 240;
			height = 160;
			translation = 1.4;
			return projection = d3.geo.mercator().scale((width + 1) / 2 / Math.PI).translate([width / 2, translation * height / 2]).precision(.1);
		};

		ZeroHome.prototype.plotPoint = function(el, size) {
			var coords, latitude, longitude;
			size = size || 'large';
			latitude = el.data('latitude');
			longitude = el.data('longitude');
			coords = this.getMapProjection()([longitude, latitude]);
			el.css({
				left: parseInt(coords[0]),
				top: parseInt(coords[1])
			}).addClass('positioned');
			return timeoutSet(450, function() {
				return el.addClass('showing');
			});
		};

		ZeroHome.prototype.createMap = function(map_style, points, map_id, line_color, bottom_padding) {
			var bounds, bounds_options, line, line_options, map, map_options, total_length;
			bottom_padding = bottom_padding || 0;
			line_color = line_color || '#444';
			line_options = {
				color: line_color,
				smoothFactor: 1.1,
				weight: 4,
				opacity: 1,
				fillOpacity: 1,
				lineCap: 'round'
			};
			map_options = {
				dragging: false,
				touchZoom: false,
				scrollWheelZoom: false,
				doubleClickZoom: false,
				boxZoom: false,
				tap: false,
				zoomControl: false,
				attributionControl: false,
				paddingBottomRight: [0, 20]
			};
			line = L.polyline(points, line_options);
			bounds = line.getBounds();
			bounds_options = {
				paddingTopLeft: [20, 20],
				paddingBottomRight: [20, bottom_padding]
			};
			map = L.mapbox.map(map_id, map_style, map_options).fitBounds(bounds, bounds_options);
			line.addTo(map);
			total_length = line._path.getTotalLength();
			$(line._path).css({
				'stroke-dasharray': total_length + 10,
				'stroke-dashoffset': total_length + 10
			});
			timeoutSet(30, function() {
				return $('#' + map_id).addClass('showing');
			});
			return map;
		};

		return ZeroHome;

	})();

	window.ZeroSport = (function() {
		function ZeroSport() {
			this.bindEvents();
			this.sportTimeouts = [];
			this.sportIntervals = [];
		}

		ZeroSport.prototype.init = function() {
			this.incrementStats();
			this.setupMri();
			this.startRevealCascade();
			return this.initializeMaps();
		};

		ZeroSport.prototype.bindEvents = function() {
			$(document).on('touchstart', '.mri', (function(_this) {
				return function(e) {
					if (_this.touchLocked) {
						return;
					}
					$('body').addClass('touching-mri');
					_this.touchLocked = true;
					return false;
				};
			})(this));
			$(document).on('touchend', '.mri', (function(_this) {
				return function(e) {
					timeoutSet(10, function() {
						return $('body').removeClass('touching-mri');
					});
					return _this.touchLocked = false;
				};
			})(this));
			$(document).on('webkitAnimationEnd', '.mri .blip', (function(_this) {
				return function(e) {
					return _this.loopScanAnimation();
				};
			})(this));
			$(document).on('click', '.switch-focus', (function(_this) {
				return function(e) {
					var target;
					e.preventDefault();
					target = $(e.currentTarget).attr('href').replace('#', '');
					return _this.switchFocus(target);
				};
			})(this));
			return $(document).on('click', '.back-to-sport', (function(_this) {
				return function(e) {
					e.preventDefault();
					return _this.removeFocus();
				};
			})(this));
		};

		ZeroSport.prototype.startRevealCascade = function() {
			return $('#page .sport-content .month').each((function(_this) {
				return function(n, el) {
					return timeoutSet(300 + 100 * n, function() {
						return $(el).addClass('showing');
					});
				};
			})(this));
		};

		ZeroSport.prototype.switchFocus = function(subsection) {
			$('body').removeClass('loaded').addClass('leaving');
			return timeoutSet(350, (function(_this) {
				return function() {
					$('body').removeClass('leaving').addClass('focus focus-' + subsection);
					return timeoutSet(20, function() {
						return $('body').addClass('loaded');
					});
				};
			})(this));
		};

		ZeroSport.prototype.removeFocus = function() {
			$('body').removeClass('loaded').addClass('leaving');
			return timeoutSet(350, function() {
				$('body').removeClass('leaving').removeClass('focus focus-gyms focus-runs focus-steps focus-bikes');
				return timeoutSet(20, function() {
					return $('body').addClass('loaded');
				});
			});
		};

		ZeroSport.prototype.setupMri = function() {
			return timeoutSet(2700, function() {
				return $('body').addClass('scan-faster');
			});
		};

		ZeroSport.prototype.loopScanAnimation = function() {
			$('.mri .blip').css({
				webkitAnimationName: 'none'
			});
			$('.mri .layer').css({
				webkitAnimationName: 'none'
			});
			return timeoutSet(8, function() {
				$('.mri .blip').css({
					webkitAnimationName: 'mri-blip'
				});
				$('.mri .layer.one').css({
					webkitAnimationName: 'mri-glow-1'
				});
				$('.mri .layer.two').css({
					webkitAnimationName: 'mri-glow-2'
				});
				$('.mri .layer.three').css({
					webkitAnimationName: 'mri-glow-3'
				});
				$('.mri .layer.four').css({
					webkitAnimationName: 'mri-glow-4'
				});
				return $('.mri .layer.five').css({
					webkitAnimationName: 'mri-glow-5'
				});
			});
		};

		ZeroSport.prototype.incrementStats = function() {
			if ($('html').hasClass('cursor')) {
				return gyroscope.aileron.timeouts.push(timeoutSet(300, (function(_this) {
					return function() {
						var ageDecimals, decimalsPerSecond;
						ageDecimals = $('#page .age-decimal-increment');
						decimalsPerSecond = Math.pow(10, 9) / (365 * 24 * 60 * 60);
						return _this.sportIntervals.push(intervalSet(70, function() {
							return _this.updateAgeDecimals(ageDecimals, decimalsPerSecond);
						}));
					};
				})(this)));
			}
		};

		ZeroSport.prototype.updateAgeDecimals = function(element, decimalsPerSecond) {
			var remainder;
			remainder = parseInt(parseFloat(element.text()) + decimalsPerSecond * 0.07) + '';
			if (remainder.length < 9) {
				return element.html('0' + remainder);
			} else {
				return element.html(remainder);
			}
		};

		ZeroSport.prototype.initializeMaps = function() {
			return $('#page .mapbox-map').each((function(_this) {
				return function(n, el) {
					return _this.initializeMap(el);
				};
			})(this));
		};

		ZeroSport.prototype.initializeMap = function(el) {
			var map, map_id, map_style, point, points, satellite_style, vector_style;
			map = $(el);
			map_id = map.attr('id');
			satellite_style = 'gyroscope.kkb54012';
			vector_style = 'gyroscope.kkbbbn8d';
			map_style = vector_style;
			if (map.hasClass('with-polyline')) {
				points = eval(map.find('.raw-points').html());
				if (points) {
					return this.createMapWithPoints(map_style, points, map_id, '#30d165', 10);
				}
			} else if (map.hasClass('with-marker')) {
				point = JSON.parse(map.find('.raw-point').html());
				if (point) {
					return this.createMapWithPoint(map_style, point, map_id, '#00aeef', 50);
				}
			}
		};

		ZeroSport.prototype.createMapWithPoints = function(map_style, points, map_id, line_color, bottom_padding) {
			var bounds, bounds_options, line, line_options, map, map_options, total_length;
			bottom_padding = bottom_padding || 0;
			line_color = line_color || '#444';
			line_options = {
				color: line_color,
				smoothFactor: 1.5,
				weight: 3,
				opacity: 1,
				fillOpacity: 0.8,
				lineCap: 'round'
			};
			map_options = {
				keyboard: false,
				dragging: false,
				touchZoom: false,
				scrollWheelZoom: false,
				doubleClickZoom: false,
				boxZoom: false,
				tap: false,
				zoomControl: false,
				attributionControl: false,
				paddingBottomRight: [0, bottom_padding]
			};
			line = L.polyline(points, line_options);
			bounds = line.getBounds();
			bounds_options = {
				paddingTopLeft: [20, 20],
				paddingBottomRight: [30, 20]
			};
			map = L.mapbox.map(map_id, map_style, map_options).fitBounds(bounds, bounds_options);
			line.addTo(map);
			total_length = line._path.getTotalLength();
			$(line._path).css({
				'stroke-dasharray': total_length + 10,
				'stroke-dashoffset': total_length + 10
			});
			timeoutSet(50, function() {
				return $('#' + map_id).addClass('showing');
			});
			return map;
		};

		ZeroSport.prototype.createMapWithPoint = function(map_style, points, map_id, line_color, bottom_padding) {
			var bounds, bounds_options, line, line_options, map, map_options;
			bottom_padding = bottom_padding || 0;
			line_color = line_color || '#444';
			line_options = {
				color: line_color,
				smoothFactor: 1.5,
				weight: 3,
				opacity: 1,
				fillOpacity: 0.8,
				lineCap: 'round'
			};
			map_options = {
				dragging: false,
				touchZoom: false,
				scrollWheelZoom: false,
				doubleClickZoom: false,
				boxZoom: false,
				tap: false,
				zoomControl: false,
				attributionControl: false,
				maxZoom: 15
			};
			line = L.polyline(points, line_options);
			bounds = line.getBounds();
			bounds_options = {
				paddingTopLeft: [40, 40],
				paddingBottomRight: [40, 40]
			};
			map = L.mapbox.map(map_id, map_style, map_options).fitBounds(bounds, bounds_options);
			timeoutSet(50, function() {
				return $('#' + map_id).addClass('showing');
			});
			return map;
		};

		return ZeroSport;

	})();

}).call(this);