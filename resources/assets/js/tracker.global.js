(function() {
	window.GyroscopeAileron = (function() {
		function GyroscopeAileron() {}

		GyroscopeAileron.prototype.init = function(site) {
			this.site = site;
			this.multiplier = 1;
			this.slow = false;
			this.currentZone = site.zone;
			this.currentSection = '';
			this.currentPage = '';
			this.currentLevel = 0;
			this.intervals = [];
			this.timeouts = [];
			this.cachedPages = {};
			this.options = {
				timeout: 9000,
				triggerClass: 'transitioned',
				pageCache: '#page-cache',
				l1Cache: '#l1-cache',
				l2Cache: '#l2-cache',
				pageContainer: '#page',
				l1Container: '.l1',
				l2Container: '.l2',
				introDuration: 1950,
				subpageIntroDuration: 1200,
				animationsPausedDuration: 400,
				unscrollableDuration: 600,
				subpageUnscrollableDuration: 400,
				mobileWidthThreshold: 750,
				leavingDuration: 650,
				subpageLeavingDuration: 400,
				slowMultiplier: 4
			};
			if ($.pjax.defaults) {
				$.pjax.defaults.scrollTo = false;
			}
			this.bindPjaxEvents();
			this.bindLoadEvents();
			this.bindHistoryEvents();
			return this.bindResizeEvents();
		};

		GyroscopeAileron.prototype.startCascade = function(depth) {
			this.clearTimeouts();
			depth = depth || 0;
			this.phase2();
			this.timeouts.push(timeoutSet(this.multiplier * 7, (function(_this) {
				return function() {
					return _this.phase3();
				};
			})(this)));
			this.timeouts.push(timeoutSet(this.multiplier * (depth ? this.options.subpageIntroDuration : this.options.introDuration), (function(_this) {
				return function() {
					return _this.phase4();
				};
			})(this)));
			this.timeouts.push(timeoutSet(this.multiplier * this.options.animationsPausedDuration, (function(_this) {
				return function() {
					return _this.phase5();
				};
			})(this)));
			return this.timeouts.push(timeoutSet(this.multiplier * (depth ? this.options.subpageUnscrollableDuration : this.options.unscrollableDuration), function() {
				return $('body').addClass('scrollable');
			}));
		};

		GyroscopeAileron.prototype.firstLoad = function() {
			this.phase1();
			if ($('#background-photo').hasClass('unset')) {
				this.site.extras.loadCoverPhoto();
				return;
			}
			return timeoutSet(5, (function(_this) {
				return function() {
					return _this.startCascade();
				};
			})(this));
		};

		GyroscopeAileron.prototype.phase1 = function() {
			return this.setUAClasses();
		};

		GyroscopeAileron.prototype.phase2 = function() {
			var el, pageClass, zone;
			el = $('#page .body-class-level-2').first();
			if (el.length !== 1) {
				el = $('#page .body-class-level-1').first();
			}
			if (el.length !== 1) {
				el = $('#page .body-class').first();
			}
			pageClass = el.val();
			zone = el.attr('data-zone');
			if (zone && zone !== this.currentZone) {
				window.location.reload();
				return;
			}
			this.previousSection = this.currentSection;
			this.previousPage = this.currentPage;
			this.previousLevel = this.currentLevel;
			this.currentSection = el.data('section');
			this.currentPage = el.data('page');
			this.currentLevel = el.data('level');
			$('body').removeClass('no-js leaving switching-page');
			return $('body').addClass(pageClass + ' level-' + this.currentLevel);
		};

		GyroscopeAileron.prototype.phase3 = function() {
			window.scrollTo(0, 1);
			$('body').removeClass('not-loaded');
			$('body').addClass('intro loaded');
			this.site.initPage(this.currentZone, this.currentSection, this.currentPage);
			if (!this.cachedPages.hasOwnProperty(window.location.pathname)) {
				return this.cachedPages[window.location.pathname] = $('#page').html();
			}
		};

		GyroscopeAileron.prototype.phase4 = function() {
			return $('body').removeClass('intro going-out going-in');
		};

		GyroscopeAileron.prototype.phase5 = function() {
			$('body').addClass('animating');
			this.slow = false;
			this.multiplier = 1;
			return $('body').removeClass('slow');
		};

		GyroscopeAileron.prototype.abortLoad = function() {
			return this.clearTimeouts();
		};

		GyroscopeAileron.prototype.clearTimeouts = function() {
			var interval, timeout, _i, _j, _len, _len1, _ref, _ref1;
			_ref = this.intervals;
			for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				interval = _ref[_i];
				clearInterval(interval);
			}
			_ref1 = this.timeouts;
			for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
				timeout = _ref1[_j];
				clearTimeout(timeout);
			}
			this.intervals = [];
			return this.timeouts = [];
		};

		GyroscopeAileron.prototype.bindLoadEvents = function() {
			return $(document).on('ready', (function(_this) {
				return function() {
					return _this.firstLoad();
				};
			})(this));
		};

		GyroscopeAileron.prototype.bindHistoryEvents = function() {
			return $(window).on('pjax:popstate', (function(_this) {
				return function() {
					_this.unsetBodyClasses();
					return timeoutSet(50, function() {
						return _this.goBack();
					});
				};
			})(this));
		};

		GyroscopeAileron.prototype.bindPjaxEvents = function() {
			this.readyToSwitch = false;
			$(document).on('click', 'a.' + this.options.triggerClass, (function(_this) {
				return function(e) {
					var link, secure, to_insecure, url;
					link = $(e.currentTarget);
					url = link.attr('href');
					secure = window.location.protocol === 'https:';
					to_insecure = url.indexOf('http://') === 0;
					if (!url || (to_insecure && secure)) {
						return;
					}
					_this.readyToSwitch = false;
					e.preventDefault();
					if (e.shiftKey) {
						_this.slow = true;
						_this.multiplier = _this.options.slowMultiplier;
					}
					return _this.delayedGoTo(url, link.data('section'), link.data('level'));
				};
			})(this));
			return $(document).on('pjax:complete', (function(_this) {
				return function() {
					return _this.readyToSwitch = true;
				};
			})(this));
		};

		GyroscopeAileron.prototype.bindResizeEvents = function() {
			$(window).resize($.debounce(150, true, (function(_this) {
				return function() {
					if (_this.orientationChanged) {
						return;
					}
					$('body').addClass('resizing');
					if (!(_this.getSizeClasses() === _this.currentSizeClasses)) {
						return _this.triggerResizeSwitch();
					}
				};
			})(this)));
			$(window).resize($.debounce(300, false, (function(_this) {
				return function() {
					if (_this.orientationChanged) {
						return;
					}
					$('body').removeClass('resizing');
					if (!(_this.getSizeClasses() === _this.currentSizeClasses)) {
						return _this.triggerResizeSwitch();
					}
				};
			})(this)));
			return $(window).on('orientationchange', (function(_this) {
				return function(e) {
					_this.orientationChanged = true;
					window.scrollTo(0, 0);
					return timeoutSet(200, function() {
						return _this.orientationChanged = false;
					});
				};
			})(this));
		};

		GyroscopeAileron.prototype.triggerResizeSwitch = function() {
			this.resetUAClasses();
			return this.setUAClasses();
		};

		GyroscopeAileron.prototype.unsetBodyClasses = function(extra) {
			if (extra == null) {
				extra = '';
			}
			if (this.slow) {
				$('body').attr('class', 'slow').prop('offsetHeight');
				$('body').addClass(extra + ' not-loaded leaving');
			} else {
				$('body').attr('class', extra + ' not-loaded leaving');
			}
			return this.site.cleanup();
		};

		GyroscopeAileron.prototype.lightlyUnsetBodyClasses = function(section, depth) {
			var direction;
			direction = 'same-level';
			if (this.currentLevel > depth) {
				direction = 'going-out';
			} else if (this.currentLevel < depth) {
				direction = 'going-in';
			}
			return $('body').attr('class', section + ' not-loaded switching-page ' + direction);
		};

		GyroscopeAileron.prototype.resetUAClasses = function() {
			return $('html, #device-info').removeClass('touch cursor mobile desktop time-x time-y');
		};

		GyroscopeAileron.prototype.setUAClasses = function(classes) {
			classes = classes || this.getUAClasses();
			return $('html, #device-info').attr('class', classes);
		};

		GyroscopeAileron.prototype.getUAClasses = function() {
			var classes, sizeClasses;
			sizeClasses = this.updateSizeClasses();
			classes = 'detected ' + $('html').data('ua');
			classes += window.hasOwnProperty('ontouchstart') ? ' touch' : ' cursor';
			return classes += ' ' + sizeClasses;
		};

		GyroscopeAileron.prototype.updateSizeClasses = function() {
			return this.currentSizeClasses = this.getSizeClasses();
		};

		GyroscopeAileron.prototype.getSizeClasses = function() {
			var classes, width;
			width = $(window).width();
			if (width < this.options.mobileWidthThreshold) {
				classes = 'mobile';
				this.isMobile = true;
			} else {
				classes = 'desktop';
				this.isMobile = false;
			}
			return classes;
		};

		GyroscopeAileron.prototype.delayedGoTo = function(url, section, depth, skipCascade, instant) {
			if (skipCascade == null) {
				skipCascade = false;
			}
			if (instant == null) {
				instant = false;
			}
			this.clearTimeouts();
			if (section === this.currentSection) {
				if (!skipCascade) {
					this.lightlyUnsetBodyClasses(section, depth);
				}
				if (depth === 2) {
					this.loadFromCacheOrPjax(url, this.options.l2Cache, this.options.l2Container);
					return this.delayedLoad = timeoutSet(this.multiplier * this.options.subpageLeavingDuration, (function(_this) {
						return function() {
							return _this.switchInterval = intervalSet(25, function() {
								return _this.tryUncache(2, skipCascade);
							});
						};
					})(this));
				} else if (depth === 1) {
					this.loadFromCacheOrPjax(url, this.options.l1Cache, this.options.l1Container);
					return this.delayedLoad = timeoutSet(this.multiplier * this.options.subpageLeavingDuration, (function(_this) {
						return function() {
							return _this.switchInterval = intervalSet(25, function() {
								return _this.tryUncache(1);
							});
						};
					})(this));
				} else {
					return this.loadTopLevel(url, 0, true);
				}
			} else {
				return this.loadTopLevel(url);
			}
		};

		GyroscopeAileron.prototype.loadTopLevel = function(url, delay, alreadyUnset) {
			if (alreadyUnset == null) {
				alreadyUnset = false;
			}
			if (!alreadyUnset) {
				this.unsetBodyClasses();
			}
			this.loadFromCacheOrPjax(url, this.options.pageCache);
			return this.delayedLoad = timeoutSet(this.multiplier * this.options.leavingDuration, (function(_this) {
				return function() {
					return _this.switchInterval = intervalSet(25, function() {
						return _this.tryUncache();
					});
				};
			})(this));
		};

		GyroscopeAileron.prototype.loadFromCacheOrPjax = function(url, container, fragment) {
			var cached, cached_page, destination;
			this.readyToSwitch = false;
			if (this.cachedPages.hasOwnProperty(url)) {
				cached = this.cachedPages[url];
				if (fragment) {
					cached_page = $(cached).find(fragment);
				} else {
					cached_page = cached;
				}
				if (cached_page.length) {
					destination = $(container);
					destination.html(cached_page);
					this.readyToSwitch = true;
					return;
				}
			}
			if (fragment) {
				return $.pjax({
					url: url,
					container: container,
					fragment: fragment,
					timeout: this.options.timeout
				});
			} else {
				return $.pjax({
					url: url,
					container: container,
					timeout: this.options.timeout
				});
			}
		};

		GyroscopeAileron.prototype.tryUncache = function(depth, skipCascade) {
			if (depth == null) {
				depth = 0;
			}
			if (skipCascade == null) {
				skipCascade = false;
			}
			if (this.readyToSwitch === true) {
				if (depth === 1) {
					this.uncacheLevel1(depth);
				} else if (depth === 2) {
					this.uncacheLevel2(skipCascade);
				} else {
					this.uncache();
				}
				return clearInterval(this.switchInterval);
			}
		};

		GyroscopeAileron.prototype.uncache = function() {
			var cache;
			cache = $(this.options.pageCache).html();
			$(this.options.pageContainer).empty().html(cache);
			$('body').removeClass('leaving switching-page switching-subpage');
			return timeoutSet(5, (function(_this) {
				return function() {
					return _this.startCascade();
				};
			})(this));
		};

		GyroscopeAileron.prototype.uncacheLevel1 = function(depth) {
			var cache;
			cache = $(this.options.l1Cache).html();
			$(this.options.l1Container).empty().html(cache);
			$('body').removeClass('leaving switching-page switching-subpage');
			return timeoutSet(5, (function(_this) {
				return function() {
					return _this.startCascade(depth);
				};
			})(this));
		};

		GyroscopeAileron.prototype.uncacheLevel2 = function(skipCascade) {
			var cache;
			cache = $(this.options.l2Cache).html();
			$(this.options.l2Container).empty().html(cache);
			$('body').removeClass('leaving switching-page switching-subpage');
			return timeoutSet(5, (function(_this) {
				return function() {
					if (!skipCascade) {
						return _this.startCascade(2);
					}
				};
			})(this));
		};

		GyroscopeAileron.prototype.goBack = function() {
			var cachedPage;
			cachedPage = this.cachedPages[window.location.pathname];
			if (cachedPage) {
				$('#page').html(cachedPage);
				$('body').removeClass('leaving level-0 level-1 level-2 switching-page switching-subpage');
				return timeoutSet(5, (function(_this) {
					return function() {
						return _this.startCascade(_this.previousLevel);
					};
				})(this));
			} else {
				return window.location.replace(window.location.toString());
			}
		};

		return GyroscopeAileron;

	})();

	window.GyroscopeBase = (function() {
		function GyroscopeBase() {
			this.connections = new GyroscopeConnections();
			this.extras = new GyroscopeExtras();
			this.bindFocus();
		}

		GyroscopeBase.prototype.init = function(zone, section, page) {
			this.zone = zone;
			this.aileron = new GyroscopeAileron();
			this.aileron.init(this);
			if (zone === 'public') {
				return this["public"] = new GyroscopePublic();
			} else if (zone === 'account') {
				return this.account = new GyroscopeAccount();
			} else if (zone === 'bloom') {
				return this.bloom = new GyroscopeBloom();
			} else if (zone === 'zero') {
				return this.zero = new GyroscopeZero();
			} else if (zone === 'seven') {
				return this.seven = new GyroscopeSeven();
			} else if (zone === 'admin') {
				return this.admin = new GyroscopeAdmin();
			}
		};

		GyroscopeBase.prototype.initPage = function(zone, section, page) {
			if (zone === 'public') {
				return this["public"].init(section, page);
			} else if (zone === 'account') {
				return this.account.init(section, page);
			} else if (zone === 'bloom') {
				return this.bloom.init(section, page);
			} else if (zone === 'zero') {
				return this.zero.init(section, page);
			} else if (zone === 'seven') {
				return this.seven.init(section, page);
			} else if (zone === 'admin') {
				return this.admin.init(section, page);
			} else if (zone === 'connect') {
				if (page === 'start') {
					return this.connections.initStart();
				} else if (page === 'end') {
					return this.connections.initEnd();
				}
			}
		};

		GyroscopeBase.prototype.cleanup = function() {};

		GyroscopeBase.prototype.bindFocus = function() {
			$(window).on('blur', (function(_this) {
				return function(e) {
					if (_this.removeFocus) {
						clearTimeout(_this.removeFocus);
					}
					return _this.removeFocus = timeoutSet(1000, function() {
						return $('body').addClass('out-of-focus');
					});
				};
			})(this));
			return $(window).on('focus', (function(_this) {
				return function(e) {
					if (_this.removeFocus) {
						clearTimeout(_this.removeFocus);
					}
					return $('body').removeClass('out-of-focus');
				};
			})(this));
		};

		return GyroscopeBase;

	})();

	window.GyroscopeConnections = (function() {
		function GyroscopeConnections() {
			this.bindRemoteConnections();
		}

		GyroscopeConnections.prototype.bindRemoteConnections = function() {
			return $(document).on('click', '.remote-connection', (function(_this) {
				return function(e) {
					var type, url;
					e.preventDefault();
					type = $(e.currentTarget).parents('li').attr('class');
					type = $(e.currentTarget).hasClass('facebook-login-button') ? !type ? 'facebook' : void 0 : void 0;
					if ($('#user-status').hasClass('added-' + type)) {
						return false;
					}
					url = $(e.currentTarget).attr('href');
					if (!url) {
						url = $(e.currentTarget).data('href');
					}
					if ($('html').hasClass('os-ios')) {
						return window.location.replace(url);
					} else {
						return _this.popupOpen(url);
					}
				};
			})(this));
		};

		GyroscopeConnections.prototype.popupOpen = function(url) {
			if (this.remote_connection) {
				this.remote_connection.close();
			}
			return this.remote_connection = window.open(url, "_blank", "height=" + window.screen.height + ",width=980,menubar=no,location=no,scrollbars=no,status=no,titlebar=no,toolbar=no");
		};

		GyroscopeConnections.prototype.initStart = function() {
			var start_url;
			start_url = $('#start_url').attr('data-url');
			if (!start_url) {
				return;
			}
			return window.location.replace(start_url);
		};

		GyroscopeConnections.prototype.initEnd = function() {
			var app, type;
			type = $('#connection_type').attr('data-type');
			app = $('#for_app').length;
			if (window.opener) {
				window.opener.gyroscope.connections.finishedConnecting(type);
				return window.close();
			} else {
				if (app) {
					return window.location.replace('gyroscope://complete/' + type + '/');
				} else if (type === 'facebook') {
					return window.location.replace('/');
				} else {
					return window.location.replace('/settings/#' + type);
				}
			}
		};

		GyroscopeConnections.prototype.finishedConnecting = function(type) {
			if (type === 'facebook') {
				return this.doFacebookLogin();
			} else {
				return this.startImport(type);
			}
		};

		GyroscopeConnections.prototype.doFacebookLogin = function() {
			var key, pageUrl, urlVar, val, _i, _len, _ref, _ref1;
			pageUrl = window.location.search.substring(1);
			_ref = pageUrl.split('&');
			for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				urlVar = _ref[_i];
				_ref1 = urlVar.split('='), key = _ref1[0], val = _ref1[_ref1.length - 1];
				if (key === 'next') {
					window.location.href = decodeURIComponent(val);
					return;
				}
			}
			return window.location.href = '/';
		};

		GyroscopeConnections.prototype.startImport = function(type) {
			return gyroscope.account.updateState();
		};

		return GyroscopeConnections;

	})();

	window.GyroscopeExtras = (function() {
		function GyroscopeExtras() {
			this.coverPhotoLoaded = false;
		}

		GyroscopeExtras.prototype.startIncrementingAge = function() {
			var ageDecimals, decimalsPerSecond;
			if ($('#device-info').hasClass('os-ios')) {
				return;
			}
			ageDecimals = $('#page .age-decimal-increment').first();
			ageDecimals.css('width', ageDecimals.width());
			decimalsPerSecond = Math.pow(10, 9) / (365 * 24 * 60 * 60);
			return gyroscope.aileron.intervals.push(intervalSet(70, (function(_this) {
				return function() {
					return _this.updateAgeDecimals($('#page .age-decimal-increment'), decimalsPerSecond);
				};
			})(this)));
		};

		GyroscopeExtras.prototype.updateAgeDecimals = function(element, decimalsPerSecond) {
			var remainder;
			remainder = parseInt(parseFloat(element.first().text()) + decimalsPerSecond * 0.07) + '';
			if (remainder.length < 9) {
				return element.html('0' + remainder);
			} else {
				return element.html(remainder);
			}
		};

		GyroscopeExtras.prototype.loadCoverPhoto = function() {
			var background, image_element, img, url;
			this.coverPhotoLoaded = false;
			background = $('.globals #background-photo');
			image_element = $('.image', background);
			if (!url) {
				url = $('#page .background_url').first().attr('data-url');
			}
			if (!url) {
				url = $('.background_url').first().attr('data-url');
			}
			if (url && (background.hasClass('unset' || image_element.attr('style') !== 'background-image: url(' + url + ')'))) {
				img = $(new Image());
				img.imagesLoaded((function(_this) {
					return function() {
						$('.image', background).attr('style', 'background-image: url(' + url + ')');
						background.removeClass('unset');
						_this.coverPhotoLoaded = true;
						gyroscope.aileron.startCascade();
						if (_this.loadTimeout) {
							return clearTimeout(_this.loadTimeout);
						}
					};
				})(this));
				img.src = url;
				return this.loadTimeout = timeoutSet(5000, (function(_this) {
					return function() {
						return gyroscope.aileron.startCascade();
					};
				})(this));
			} else {
				if (!url) {
					background.addClass('no-image');
					background.find('shadowing').remove();
				}
				this.coverPhotoLoaded = true;
				return gyroscope.aileron.startCascade();
			}
		};

		GyroscopeExtras.prototype.gridSetup = function() {
			var classes, columns, delay, dots, grid, initialDelay, rows, x, xPos, xSpeed, y, yPos, ySpeed, _i, _j;
			grid = $('.grid .dots');
			columns = 24;
			rows = 16;
			dots = '';
			xSpeed = 40;
			ySpeed = 60;
			xSpeed = 24;
			ySpeed = 24;
			xSpeed = 19;
			ySpeed = 28;
			initialDelay = 10;
			initialDelay = 100;
			for (x = _i = 0; _i <= columns; x = _i += 1) {
				for (y = _j = 0; _j <= rows; y = _j += 1) {
					xPos = x * (100 / columns);
					yPos = y * (100 / rows);
					delay = initialDelay + ((xSpeed * columns) - xSpeed * Math.abs(x - columns / 2)) + ((ySpeed * rows) - ySpeed * Math.abs(y - rows / 2));
					delay = initialDelay + (xSpeed * Math.abs(x - columns / 2)) + (ySpeed * Math.abs(y - rows / 2));
					classes = '';
					if (y > 14) {
						classes += 'below';
					}
					dots += '<span class="dot ' + classes + '" style="top:' + yPos + '%; left:' + xPos + '%; transition-delay:' + delay + 'ms" />';
				}
			}
			grid.append($(dots));
			return grid.addClass('initialized');
		};

		GyroscopeExtras.prototype.mouseWatch = function() {
			var throttleAmount;
			this.body = $('body');
			this.pageWidth = $(window).width();
			this.pageHeight = $(window).height();
			throttleAmount = 133;
			throttleAmount = 16;
			return $(window).mousemove($.throttle(throttleAmount, (function(_this) {
				return function(e) {
					return _this.analyzeMouseMovement(e);
				};
			})(this)));
		};

		GyroscopeExtras.prototype.analyzeMouseMovement = function(e) {
			var mouseX, mouseY;
			mouseX = 100 - (100 * e.pageX / this.pageWidth);
			mouseY = 100 * e.pageY / this.pageHeight;
			return this.setOrigin(mouseX, mouseY);
		};

		GyroscopeExtras.prototype.setOrigin = function(x, y) {
			return this.body.css({
				'-webkit-perspective-origin': x + '% ' + y + '%'
			});
		};

		return GyroscopeExtras;

	})();

	window.timeoutSet = function(time, fn) {
		return setTimeout(fn, time);
	};

	window.intervalSet = function(time, fn) {
		return setInterval(fn, time);
	};

	window.numberWithCommas = function(num) {
		return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	};

	window.padWithZeros = function(n, digits) {
		n = n + '';
		if (n.length >= digits) {
			return n;
		} else {
			return new Array(digits - n.length + 1).join('0') + n;
		}
	};

}).call(this);