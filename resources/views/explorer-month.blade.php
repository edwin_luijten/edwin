<!DOCTYPE html>
<html class="not-detected" data-ua="browser-chrome os-windows-7 device-other">
    @include('partials.head')

    <body class="not-loaded">

    <div id="device-info">
            <span class="background"></span>

            <main class="page" id="page">
                <div class="column-2 backdrop"></div>
                <div class="wide column-3 backdrop for-explorer"></div>

                <div class="l1 level-1-container">
                    <div class="wide column-3"></div>
                    <div class="full-column">
                        <div class="month-header medium">
                            <a class="back transitioned uplevel" data-level="0" data-section="explorer" data-page="months" href="/explorer/">
                                <span class="arrow">‹</span>

                                <span class="section-title">Explorer</span>
                            </a>

                            <div class="container">
                                <div class="title">
                                    <h1>{{ $month }}</h1>
                                    <h2>Explored {{ $nPlaces }} places</h2>
                                </div>
                                <ul class="locations-listing"></ul>
                                <div class="honeycomb">
                                    <?php
                                        foreach($headerPlaces as $col => $places)
                                        {
                                            ?>
                                            <div class="hexes-{{ $col }} column">
                                                <?php
                                                    foreach($headerPlaces[$col] as $place)
                                                    {
                                                        $day = Carbon\Carbon::parse($place['startTime'])->format('d');
                                                        $tags = '';

                                                        foreach($place['place']['foursquare']['tags'] as $tag)
                                                        {
                                                            $tags.= 'category-' . $tag . ' ';
                                                        }
                                                        ?>
                                                        <a href="#day-{{ $day }}" class="hex {{ $tags }}">
                                                            <div class="jitter">
                                                                <div class="shape tintable"></div>
                                                                <background class="symbol" style="background-image: url({{ $place['place']['foursquare']['primaryCategory']['icon']['prefix'] }}32{{ $place['place']['foursquare']['primaryCategory']['icon']['suffix'] }});"></background>
                                                            </div>
                                                            <span class="text"><strong>{{ $place['place']['name'] }}</strong><small></small></span>
                                                        </a>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="month-content medium-header">
                            <span class="backdrop"></span>
                            <div class="month-days scrollable">
                                <div class="beginning month-pagination">
                                    <span class="top-line"></span>
                                    <a class="previous-month transitioned" href="/explorer/{{ Illuminate\Support\Str::slug($prevMonth) }}/" data-level="1" data-section="explorer">
                                        <span class="left arrow"></span>
                                        <span class="label">Previous Month</span>
                                    </a>
                                </div>

                                <?php $index = 34; ?>
                                @foreach($placesThisMonth as $day => $items)

                                    <div class="day" id="day-{{ \Carbon\Carbon::parse($day)->format('d') }}" style="z-index: {{ $index }};">
                                        <span class="hovering"></span>
                                        <header class="day-info">
                                            <span class="day-number">{{ \Carbon\Carbon::parse($day)->format('d') }}</span>
                                            <span class="day-of-week">{{ \Carbon\Carbon::parse($day)->format('l') }}</span>
                                        </header>
                                        <header class="city-info">
                                            <span class="city-name"></span>
                                            <span class="line"></span>
                                        </header>

                                        <div class="timeline has-visits">
                                            <span class="timeline-backdrop"></span>
                                            <span class="blurred-timeline-backdrop"></span>
                                            <span class="core-line"></span>

                                            <div class="visits">
                                                <?php
                                                    $i = 0;
                                                    $max = count($items);
                                                    $middle = $max / 2;
                                                    foreach($items as $time => $place)
                                                    {

                                                        if(empty($place['category']))
                                                        {
                                                            continue;
                                                        }

                                                        $i++;

                                                        $tags = '';

                                                        $css = '';

                                                        if($place['start'] == 00.00)
                                                        {
                                                            $css.='at-beginning ';
                                                        }

                                                        if($place['start'] < 12.00)
                                                        {
                                                            $css.='in-morning ';
                                                        }

                                                        foreach($place['category']['tags'] as $tag)
                                                        {
                                                            $tags.= 'category-' . Illuminate\Support\Str::slug($tag) . ' ';
                                                        }
                                                    ?>
                                                        <div class="visit from-moves {{ $tags }} {{ $place['cssMoment'] }} {{ $place['cssDuration'] }} {{ $css }}" style="top: {{ $place['startPercentage'] }}%; height: {{ $place['durationPercentage'] }}%;" data-start="{{ $place['start'] }}" data-venue="venue-{{ $i }}">
                                                            <span class="circle tintable">
                                                                <img class="square" src="/assets/images/zero/explorer/square.png">
                                                                <span class="icon" style="background-image: url({{ $place['category']['primaryCategory']['icon']['prefix'] }}32{{ $place['category']['primaryCategory']['icon']['suffix'] }});"></span>
                                                            </span>
                                                            <span class="tintable-text label" style="-webkit-transform: rotateZ({{ $place['labelRotation'] }}deg) translateY(-65%);">
                                                                <span class="venue-name">{{ $place['name'] }}</span>
                                                            </span>
                                                            <span class="time-info">
                                                                <span class="start-time">{{ $place['start'] }}</span>
                                                                <span class="duration tintable-text">{{ $place['duration']  }} hrs</span>
                                                            </span>
                                                        </div>
                                                <?php
                                                    }
                                                ?>
                                            </div>
                                            <div class="travels">
                                                <span class="divider"></span>
                                                @if(!empty($travelsThisMonth))
                                                    @if(!empty($travelsThisMonth[$day]))
                                                        @foreach($travelsThisMonth[$day] as $item)
                                                            <div class="travel from-moves type-{{ $item['type'] }} {{ $item['cssMoment'] }} {{ $item['cssDuration'] }}" style="top: {{ $item['startPercentage'] }}%; height: {{ $item['durationPercentage'] }}%;" data-start="{{ $item['start'] }}">
                                                                <span class="line"></span>
                                                                <div class="travel-info">
                                                                    <span class="travel-type">{{ ucwords($item['type']) }}</span>
                                                                    <div style="transition-delay: 465ms;"><span class="duration">{{ round($item['duration'], 2) }} min</span></div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </div>

                                        </div>
                                        <div class="details"></div>
                                    </div>
                                    <?php $index--; ?>
                                @endforeach
                                <div class="ending month-pagination">
                                    <span class="top-line"></span>
                                    <a class="mobile-only previous-month transitioned" href="/explorer/{{ Illuminate\Support\Str::slug($nextMonth) }}/" data-level="1" data-section="explorer">
                                        <span class="left arrow"></span>
                                        <span class="label">Previous Month</span>
                                    </a>
                                    <a class="next-month transitioned" href="/explorer/{{ Illuminate\Support\Str::slug($nextMonth) }}/" data-level="1" data-section="explorer">
                                        <span class="label">Next Month</span>
                                        <span class="right arrow"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input class="body-class" type="hidden" value="explorer month" data-section="explorer" data-page="month" data-level="1" data-zone="zero"/>
            </main>

            <!-- Cache -->
            <div class="hidden" id="page-cache"></div>
            <div class="hidden" id="l1-cache"></div>
            <div class="hidden" id="l2-cache"></div>
            <!-- End Cache -->

            <script type="text/javascript">
                // BEGIN - Javascript CSRF (security) integration
                function csrfSafeMethod(method) {
                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
                }
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                var csrftoken = getCookie('csrftoken');
                $.ajaxSetup({
                    beforeSend: function(xhr, settings) {
                        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        }
                    }
                });
                // END - Javascript CSRF (security) integration

                $(function() {
                    window.gyroscope = new GyroscopeBase();
                    gyroscope.init('zero', 'explorer', 'month')
                })
            </script>
        </div>
    </body>
</html>