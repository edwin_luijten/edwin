<!DOCTYPE html>
<html class="not-detected" data-ua="browser-chrome os-windows-7 device-other">
    @include('partials.head')

    <body class="not-loaded">

    <div id="device-info">
            <span class="background"></span>

            <main class="page" id="page">
                <div class="column-2 backdrop"></div>
                <div class="column-3 backdrop for-sport"></div>
                <div class="section-header backdrop for-sport"></div>
                <div class="sport-columns">
                    <div class="column-1">
                        <a class="uplevel pjax" data-section="home" href="/">
                            <span class="arrow">‹</span>
                            <strong class="user-name">
                               Edwin
                            </strong>

                            <h1 class="section-title">Sport</h1>

                            <span class="smaller alive icon sport-icon">
                                <span class="bar one"></span>
                                <span class="bar two"></span>
                                <span class="bar three"></span>
                            </span>
                        </a>

                        <div class="hoverable">
                            <dl class="sport-bloodwork">
                                <h3 class="mobile-only">Blood Levels</h3>

                                <dt>Vitamin D</dt>
                                <dd>
                                    <span class="value">25.9 <span class="unit">ng/mL</span></span>
                                    <span class="low bar"><span class="fill" style="width: 20%"></span></span>
                                    <span class="critical low alert"><span class="icon"></span><span class="text">Low</span></span>
                                </dd>
                                <dt>HDL</dt>
                                <dd>
                                    <span class="value">58 <span class="unit">mg/dL</span></span>
                                    <span class="low bar"><span class="fill" style="width: 30%"></span></span>
                                    <span class="low alert"><span class="icon"></span><span class="text">Low</span></span>
                                </dd>
                                <dt>LDL</dt>
                                <dd>
                                    <span class="value">87 <span class="unit">mg/dL</span></span>
                                    <span class="high bar"><span class="fill" style="width: 80%"></span></span>
                                    <span class="high alert"><span class="icon"></span><span class="text">High</span></span>
                                </dd>
                            </dl>
                        </div>
                    </div>

                    <div class="column-2 for-sport">
                        <dl class="bigger stats">
                            <div class="heart-rate history hoverable">
                                <div class="behind-1"><div class="updated">Last measured 9&nbsp;hours ago</div>
                                <div class="diff ">
                                    <span class="amount"><span class="arrow">▼</span>12</span>
                                    <span class="time">In 7 hours</span>
                                </div>
                                <div class="graph">

                                </div>
                            </div>
                        </div>
                        <div class="weight history hoverable">
                            <dt>Weight</dt>
                            <dd>
                                <span class="incrementable weight-increment processed" style="width: 70px;">75</span>
                                <span class="units">kg</span>
                            </dd>
                        </div>

                        <div class="bodyfat">
                            <dt>Bodyfat</dt>
                            <dd>
                                <span class="incrementable bodyfat-increment processed" style="width: 54px;">18.4</span>
                                <span class="units">%</span>
                            </dd>
                        </div>

                        <div class="age">
                            <dt>Age</dt>
                            <dd>
                                <span class="incrementable age-increment processed" style="width: 31px;">{{ 26 }}</span>
                                <span class="age-decimal">
                                    .<span class="incrementable age-decimal-increment processed" style="width: 54px;">{{ round(microtime(true) * 1000) }}</span>
                                </span>
                            </dd>
                        </div>
                    </dl>
                    </div>

                    <div class="mri mobile-only">
                        <span class="grey layer"></span>
                        <span class="layer one"></span>
                        <span class="layer two"></span>
                        <span class="layer three"></span>
                        <span class="layer four"></span>
                        <span class="layer five"></span>
                        <span class="blip">
                            <span class="cover-below"></span>
                        </span>
                    </div>

                    <div class="sport-nav mobile-only">

                    </div>

                    <div class="column-3 for-sport">
                        <header class="sport-header section-header">

                        </header>

                        <div class="sport-content">
                            @foreach($activitiesPerMonth as $month => $log)

                                <div class="month">
                                    <h2>{{ \Carbon\Carbon::parse($month)->format('F Y') }}</h2>

                                    <?php
                                    // Create groups of activities
                                    $groups = [];
                                    $totalWalking = 0;
                                    $totalCycling = 0;
                                    $totalSteps = 0;
                                    foreach($log as $activities)
                                    {
                                        if(!empty($activities['segments']))
                                        {
                                            foreach($activities['segments'] as $segment)
                                            {
                                                $steps = 0;
                                                $distance = 0;
                                                foreach($segment['activities'] as $activity)
                                                {
                                                    if($activity['activity'] === 'transport')
                                                    {
                                                        continue;
                                                    }

                                                    $distance = $distance + $activity['distance'];
                                                    $item['distance'] = $distance / 1000;

                                                    if($item['distance'] <= 0.00)
                                                    {
                                                        unset($item['distance']);
                                                        continue;
                                                    }

                                                    if($activity['activity'] === 'walking')
                                                    {
                                                        $steps = $steps + $activity['steps'];
                                                        $totalSteps = $totalSteps + $steps;

                                                        $item['steps'] = $steps;
                                                        $totalWalking = $totalWalking + $distance;
                                                    }
                                                    else if($activity['activity'] === 'cycling')
                                                    {
                                                        unset($item['steps']);
                                                        $totalCycling = $totalCycling + $distance;
                                                    }
                                                    else
                                                    {
                                                        unset($item['steps']);
                                                    }

                                                    $item['duration'] = $activity['duration'];

                                                    $item['kmu'] = round(round($distance / 1000, 2) /  round($activity['duration'] / 60, 2) * 60, 2);
                                                    $item['width'] = round($distance / 100, 2, PHP_ROUND_HALF_EVEN );

                                                    $d = \Carbon\Carbon::parse($activity['startTime'])->format('Ymd');
                                                    $groups[$activity['activity']][$d] = $item;
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="walks movement activities">
                                        <h3>Walked {{ number_format($totalWalking / 1000, 2) }} km</h3>

                                            @if(!empty($groups['walking']))
                                                @foreach($groups['walking'] as $day => $meta)

                                                <div class="motion run {{ speedComparison('walking', $meta['kmu']) }}">
                                                    <a class="go-to-day" data-section="explorer" data-level="1" href="{{ URL::route('explorer.month',  ['month' => strtolower(\Carbon\Carbon::parse($month)->format('F-Y'))]) }}/">
                                                    <div class="fill" style="width: {{ $meta['width'] }}%;">
                                                        <span class="bar"></span>
                                                        <span class="distance">{{ number_format($meta['distance'], 2) }} km</span>
                                                        <span class="pace">{{ $meta['kmu'] }} km/u</span>
                                                    </div>
                                                    </a>
                                                    <div class="motion-details">
                                                        <span class="date">{{ \Carbon\Carbon::parse($day)->formatLocalized('%A, %B %d') }}</span>
                                                        <span class="duration">Walked for {{ \Carbon\Carbon::createFromTimestamp($meta['duration'])->format('H:i:s') }}  minutes</span>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif

                                    </div>
                                    <div class="runs movement activities">
                                        <h3>Cycled {{ number_format($totalCycling / 1000, 2) }} km</h3>
                                        <div class="runs-list">
                                            @if(!empty($groups['cycling']))
                                                @foreach($groups['cycling'] as $day => $meta)

                                                <div class="motion run {{ speedComparison('cycling', $meta['kmu']) }}">
                                                    <a class="go-to-day" data-section="explorer" data-level="1" href="{{ URL::route('explorer.month',  ['month' => strtolower(\Carbon\Carbon::parse($month)->format('F-Y'))]) }}/">
                                                    <div class="fill" style="width: {{ $meta['width'] }}%;">
                                                        <span class="bar"></span>
                                                        <span class="distance">{{ number_format($meta['distance'], 2) }} km</span>
                                                        <span class="pace">{{ $meta['kmu'] }} km/u</span>
                                                    </div>
                                                    </a>
                                                    <div class="motion-details">
                                                        <span class="date">{{ \Carbon\Carbon::parse($day)->formatLocalized('%A, %B %d') }}</span>
                                                        <span class="duration">Cycled for {{ \Carbon\Carbon::createFromTimestamp($meta['duration'])->format('H:i:s') }}  minutes</span>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="steps activities">
                                        <h3>{{ $totalSteps }} steps</h3>
                                    </div>
                                </div>


                            @endforeach
                            <div class="footer">
                                <div class="container">
                                    <p class="copyright">
                                        © 2015 Luijten·
                                    </p>
                                    <p class="attribution-info">Venue icons from Foursquare · Maps from Mapbox</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input class="body-class" type="hidden" value="sport" data-section="sport" data-page="" data-level="0" data-zone="zero" />
            </main>

            <!-- Cache -->
            <div class="hidden" id="page-cache"></div>
            <div class="hidden" id="l1-cache"></div>
            <div class="hidden" id="l2-cache"></div>
            <!-- End Cache -->

            <script type="text/javascript">
                // BEGIN - Javascript CSRF (security) integration
                function csrfSafeMethod(method) {
                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
                }
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                var csrftoken = getCookie('csrftoken');
                $.ajaxSetup({
                    beforeSend: function(xhr, settings) {
                        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        }
                    }
                });
                // END - Javascript CSRF (security) integration

                $(function() {
                    window.gyroscope = new GyroscopeBase();
                    gyroscope.init('zero', 'sport', '')
                })
            </script>
        </div>
    </body>
</html>