<!DOCTYPE html>
<html class="not-detected" data-ua="browser-chrome os-windows-7 device-other">
    @include('partials.head')

    <body class="not-loaded">

    <div id="device-info">
            <span class="background"></span>

            <main class="page" id="page">
                <div class="column-2 backdrop"></div>
                <div class="wide column-3 backdrop for-explorer"></div>
                <div class="wide section-header backdrop"></div>

                <div class="column-1">
                    <a class="uplevel pjax" data-section="home" href="/">
                        <span class="arrow">‹</span>
                        <strong class="user-name">
                            Edwin
                        </strong>

                        <h1 class="section-title">Explorer</h1></a>
                </div>

                <div class="l1 level-1-container">
                    <div class="wide column-3">
                        <header class="wide section-header explorer-header">
                            <div class="map-container">
                            <a class="blip showing"
                            data-latitude="{{ $lastLocation['place']['location']['lat'] }}" data-longitude="{{ $lastLocation['place']['location']['lon'] }}"
                            data-section="explorer"
                            data-level="1"
                            href="/explorer/{{ $currentMonthSlug }}/"><span class="circle-1"></span><span class="circle-2"></span><span class="circle-3"></span><span class="circle"><img class="symbol" src="{{ (empty($lastLocation['place']['foursquare']['primaryCategory']['icon']['prefix']) ? 'https://foursquare.com/img/categories_v2/building/default_' : $lastLocation['place']['foursquare']['primaryCategory']['icon']['prefix']) }}32{{ (empty($lastLocation['place']['foursquare']['primaryCategory']['icon']['suffix']) ? '.png' : $lastLocation['place']['foursquare']['primaryCategory']['icon']['suffix']) }}"></span></a>


                                <div class="map-goes-here"></div>
                            </div>

                            <div class="current-location ">
                                <h3>Right Now</h3>

                                <h1>
                                    <a class="go-to-day" data-section="explorer" data-page="month" data-level="1" href="/explorer/october-2014/07/">Naarden</a>
                                </h1>

                                <div class="venue-info">
                                    <div class="lat-long">
                                        <span class="lat">
                                            <small>Latitude</small>
                                            <span class="value">52.2958300</span>
                                        </span>
                                        <span class="long">
                                            <small>Longitude</small>
                                            <span class="value">5.1625000</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </header>

                        <section class="content">
                            <div class="layout-list">
                                <div class="months-list">
                                    @foreach($placesPerMonth as $month => $log)

                                    <div class="month">
                                        <div class="month-title">
                                            <h2>{{ $month }}</h2>

                                            <ul class="cities">
                                                @if(!empty($log['cities']))
                                                    <?php
                                                        $maxCities = 4;
                                                        $nCities = count($log['cities']);
                                                        $i=0;
                                                    ?>

                                                    @foreach($log['cities'] as $city)
                                                        <?php $i++; ?>
                                                        @if($i <= $maxCities)
                                                            <li class="city">{{ $city }} ·  </li>
                                                        @else
                                                            <li class="city more">and {{ ($nCities - $maxCities) }} more</li>
                                                            <?php break; ?>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>

                                        <div class="month-categories">
                                            <?php
                                            $maxPlaces = 4;
                                            $nPlaces = count($log['places']);
                                            $i=0;
                                            ?>
                                            @foreach($log['places'] as $place)
                                                <?php $i++; ?>
                                                @if($i <= $maxPlaces)
                                                <div class="category">
                                                    <div class="symbol">
                                                        <img src="{{ $place['foursquare']['primaryCategory']['icon']['prefix'] }}64{{ $place['foursquare']['primaryCategory']['icon']['suffix'] }}" class="colorized">
                                                    </div>

                                                    <div class="text">
                                                        <strong>{{ $place['foursquare']['primaryCategory']['name'] }}</strong>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach

                                        </div>

                                        <a data-section="explorer" data-level="1" class="link-overlay" href="{{ URL::route('explorer.month', ['month' => strtolower(\Carbon\Carbon::parse($month)->format('F-Y'))]) }}"></a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <input class="body-class" type="hidden" value="explorer" data-section="explorer" data-page="months" data-level="0" data-zone="zero" />
            </main>

            <!-- Cache -->
            <div class="hidden" id="page-cache"></div>
            <div class="hidden" id="l1-cache"></div>
            <div class="hidden" id="l2-cache"></div>
            <!-- End Cache -->

            <script type="text/javascript">
                // BEGIN - Javascript CSRF (security) integration
                function csrfSafeMethod(method) {
                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
                }
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                var csrftoken = getCookie('csrftoken');
                $.ajaxSetup({
                    beforeSend: function(xhr, settings) {
                        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        }
                    }
                });
                // END - Javascript CSRF (security) integration

                $(function() {
                    window.gyroscope = new GyroscopeBase();
                    gyroscope.init('zero', 'explorer', '')
                })
            </script>
        </div>
    </body>
</html>