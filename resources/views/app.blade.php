<!DOCTYPE html>
<html class="not-detected" data-ua="browser-chrome os-windows-7 device-other">
    @include('partials.head')

    <body class="not-loaded">

    <div id="device-info">
            <span class="background"></span>

            <main class="page" id="page">
                <!-- Logo -->
                <h1 class="user-name for-home">
                    <span class="name">Edwin Luijten</span>
                </h1>
                <!-- End Logo -->

                <!-- Navigation -->
                <ul class="sections-nav">
                    <span class="selection cursor-only"></span>
                    <li class="sport">
                        <a data-section="sport" class="transitioned" href="/sport/">
                        <strong>Sport</strong>
                            <span class="alive icon sport-icon">
                            <span class="bar one"></span>
                            <span class="bar two"></span>
                            <span class="bar three"></span>
                        </span>
                        <small>Fitness &amp; health&nbsp;tracking</small>
                        </a>
                    </li>
                    <li class="explorer">
                        <a data-section="explorer" class="transitioned" href="{{ URL::route('explorer') }}/">
                        <strong>Explorer</strong>
                        <small>Adventures around the&nbsp;world</small>
                        </a>
                    </li>
                </ul>
                <!-- End Navigation -->

                <!-- Content -->
                <div class="spinner stationary desktop-only">
                    <div class="sport-spinner-content content-preview">
                        <div class="content-preview-plane mapboxed">
                            <div class="map-card run-card">
                                <span class="big run-map leaflet-container leaflet-fade-anim showing mapbox-map" id="homepage-run-map" tabindex="0">
                                    <div class="raw-points" style="display: none">
                                        @if($points !== '[]')
                                            {!! $points !!}
                                        @else
                                            [[ {!! $lastLocation['place']['location']['lat'] !!}, {!! $lastLocation['place']['location']['lon'] !!} ]]
                                        @endif
                                    </div>
                                </span>

                            <div class="details">
                                <h3>{{ $totalDistance }}km</h3>
                                <h4>{{ $totalDuration }}</h4>
                                <a class="pjax all" href="/sport/" data-section="sport">See all</a>
                            </div>
                            </div>


                        </div>

                        <a class="goto pjax sport" href="/sport/" data-section="sport"></a>

                    </div>

                    <div class="explorer-spinner-content content-preview">

                        <div class="map-container">
                            <div id="map-goes-here" class="map-goes-here">
                                <div class="the-map"></div>
                                <span class="blip location-coordinates" data-latitude="{{ $lastLocation['place']['location']['lat'] }}" data-longitude="{{ $lastLocation['place']['location']['lon'] }}">
                                    <span class="blip-ring one"></span>
                                    <span class="blip-ring two"></span>
                                    <span class="blip-ring three"></span>
                                    <span class="center">
                                        <span></span>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class="location-details">
                            <h3 class="location-city">{{ $lastLocation['place']['name'] }}</h3>
                            <h4 class="location-time-ago">{{ \Carbon\Carbon::parse($lastLocation['lastUpdate'])->diffForHumans() }}</h4>
                        </div>

                        <a class="goto pjax explorer" href="/explorer/" data-section="explorer"></a>

                    </div>

                    <div class="explorer top-circle-contents">
                        <small>Currently at</small>
                        <span class="location-name ">{{ $lastLocation['place']['name'] }}</span>
                        <span class="location-icon">

                            <img src="{{ (empty($lastLocation['place']['foursquare']['primaryCategory']['icon']['prefix']) ? 'https://foursquare.com/img/categories_v2/building/default_' : $lastLocation['place']['foursquare']['primaryCategory']['icon']['prefix']) }}32{{ (empty($lastLocation['place']['foursquare']['primaryCategory']['icon']['suffix']) ? '.png' : $lastLocation['place']['foursquare']['primaryCategory']['icon']['suffix']) }}">
                        </span>
                    </div>

			    </div>

			    <div class="spinner spinning desktop-only">
                    <span class="rings">
                        <span class="group-1">
                            <span class="ring zero"></span>
                        </span>
                        <span class="ring one"></span>
                        <span class="group-2">
                            <span class="ring two"></span>
                        </span>
                        <span class="group-3">
                            <span class="ring three"></span>
                            <span class="ring four"></span>
                        </span>
                    </span>

                    <div class="sport-preview desktop-only">
                        <span class="mini-preview">
                            <span class="symbol">
                                <span class="text">Latest Run</span>
                            </span>
                        </span>
                        <a class="full-preview sport sport-circles" data-section="sport" href="/sport/">
                            <span class="run circle backdrop"></span>
                            <span class="run full circle">
                                <span class="the-circle"></span>
                                <div class="circle-contents">
                                <strong>{{ $walkingSteps }}</strong>
                                <small>Steps</small>
                                    </div>
                            </span>

                            <span class="second circle backdrop"></span>
                            <span class="second full circle">
                                 <span class="the-circle"></span>
                                <div class="circle-contents">
                                 <strong class="big-number">{{ round($cyclingTotalDistance, 0) }}</strong>
                                 <small>Cycling</small>
                                </div>
                            </span>
                        </a>
                    </div>

                    <div class="explorer-preview desktop-only">
                        <span class="mini-preview">
                            <span class="symbol">
                                <span class="text">Where am I?</span>
                            </span>
                        </span>
                        <a class="full-preview explorer" data-section="explorer" href="/explorer/">
                            <span class="big-circle backdrop"></span>
                            <span class="big-circle actual"></span>
                        </a>
                    </div>

                </div>
                <!-- End Content -->

                <input class="body-class" type="hidden" value="home" data-section="home" data-page="" data-level="0" data-zone="zero" />
            </main>

            <!-- Cache -->
            <div class="hidden" id="page-cache"></div>
            <div class="hidden" id="l1-cache"></div>
            <div class="hidden" id="l2-cache"></div>
            <!-- End Cache -->

            <script type="text/javascript">
                // BEGIN - Javascript CSRF (security) integration
                function csrfSafeMethod(method) {
                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
                }
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                var csrftoken = getCookie('csrftoken');
                $.ajaxSetup({
                    beforeSend: function(xhr, settings) {
                        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                            xhr.setRequestHeader("X-CSRFToken", csrftoken);
                        }
                    }
                });
                // END - Javascript CSRF (security) integration

                $(function() {
                    window.gyroscope = new GyroscopeBase();
                    gyroscope.init('zero', 'home', '')
                })
            </script>
        </div>
    </body>
</html>