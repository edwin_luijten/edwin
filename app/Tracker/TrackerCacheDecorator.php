<?php namespace App\Tracker;

use App\Services\Cache\CacheInterface;

class TrackerCacheDecorator extends AbstractTrackerDecorator
{
	protected $cache;
	public $fresh = false;

	public function __construct(Tracker $tracker, CacheInterface $cache)
	{
		parent::__construct($tracker);

		$this->cache = $cache;
	}

	public function getSummary()
	{
		$key = md5('summary');

		if ( $this->cache->has($key) AND $this->fresh === false)
		{
			return $this->cache->get($key);
		}

		$summary = $this->tracker->getSummary();

		$this->cache->put($key, $summary, 15);

		return $summary;
	}

	public function getSport()
	{
		$key = md5('sport');

		if ( $this->cache->has($key) AND $this->fresh === false)
		{
			return $this->cache->get($key);
		}

		$sport = $this->tracker->getSport();

		$this->cache->put($key, $sport);

		return $sport;
	}

	public function getExplorer()
	{
		$key = md5('explorer');

		if ( $this->cache->has($key) AND $this->fresh === false )
		{
			return $this->cache->get($key);
		}

		$explorer = $this->tracker->getExplorer();

		$this->cache->put($key, $explorer);

		return $explorer;
	}

	public function getExplorerMonth($month)
	{
		$key = md5('explorer_' . $month);

		if ( $this->cache->has($key) AND $this->fresh === false )
		{
			return $this->cache->get($key);
		}

		$explorerMonth = $this->tracker->getExplorerMonth($month);

		$this->cache->put($key, $explorerMonth);

		return $explorerMonth;
	}
}