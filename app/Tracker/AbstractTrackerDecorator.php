<?php namespace App\Tracker;

abstract class AbstractTrackerDecorator {

    protected $tracker;

    public function __construct(Tracker $tracker)
    {
        $this->tracker = $tracker;
    }

    public function getSummary()
    {
        return $this->tracker->getSummary();
    }

    public function getSport()
    {
        return $this->tracker->getSport();
    }

    public function getExplorer()
    {
        return $this->tracker->getExplorer();
    }

    public function getExplorerMonth($month)
    {
        return $this->tracker->getExplorerMonth($month);
    }
}