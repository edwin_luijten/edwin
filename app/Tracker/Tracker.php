<?php namespace App\Tracker;

use App\Moves\Moves;
use Carbon\Carbon;

class Tracker
{
	private $api;
	private $venue;
	private $moves;
	private $profile;

	public function __construct()
	{
		// Get the api decorated api instance
		$this->api = app('App\Moves\Api');
		$this->venue = app('App\Foursquare\Venue');
		$this->moves = new Moves();
		$this->profile = $this->api->getProfile();
	}

	/**
	 * @return array
	 */
	public function getSummary()
	{
		$this->api->fresh = true;
		$dailyPlaces = $this->api->getDailyPlaces();
		$dailyStoryline = $this->api->getDailyStoryline(['trackPoints' => 'true']);

		if ( empty(end($dailyStoryline)['segments']) )
		{
            $results = $this->goBackInTime();

			$dailyPlaces = $results[0];
			$dailyStoryline = $results[1];
		}

		$lastStoryItem = end($dailyStoryline);

		$cyclingTotalDistance = 0;
		$walkingTotalDistance = 0;
		$walkingSteps = 0;
		$totalDuration = 0;

        if(!empty($lastStoryItem['summary'])) {
            foreach ($lastStoryItem['summary'] as $summary) {
                if ($summary['activity'] === 'cycling') {
                    $cyclingTotalDistance = $cyclingTotalDistance + $summary['distance'];
                }

                if ($summary['activity'] === 'walking') {
                    $walkingTotalDistance = $walkingTotalDistance + $summary['distance'];
                    $walkingSteps = $walkingSteps + $summary['steps'];
                }

                $totalDuration = $totalDuration + $summary['duration'];
            }
        }
		$points = '[';

        if(!empty($lastStoryItem['segments'])) {
            foreach ($lastStoryItem['segments'] as $segment) {
                if ($segment['type'] === 'move') {
                    if (!empty($segment['activities'])) {
                        foreach ($segment['activities'] as $activity) {
                            if (!empty($activity['trackPoints'])) {
                                foreach ($activity['trackPoints'] as $trackPoint) {
                                    $points .= '[' . $trackPoint['lat'] . ', ' . $trackPoint['lon'] . '],';
                                }
                            }
                        }
                    }
                }
            }
        }
		$points .= ']';

		return [
			'lastLocation'         => end(end($dailyPlaces)['segments']),
			'points'               => $points,
			'cyclingTotalDistance' => number_format($cyclingTotalDistance / 1000, 2),
			'walkingTotalDistance' => number_format($walkingTotalDistance / 1000, 2),
			'totalDistance'        => number_format(($cyclingTotalDistance + $walkingTotalDistance) / 1000, 2),
			'walkingSteps'         => $walkingSteps,
			'totalDuration'        => Carbon::createFromTimestamp($totalDuration)->toTimeString(),
		];
	}

	/**
	 * @return array
	 */
	public function getSport()
	{
		// Find number of months passed sins registration
		$then = Carbon::parse($this->profile['profile']['firstDate']);
		$now = Carbon::now();
		$months = $now->diffInMonths($then);

		$activitiesPerMonth = [];

		// This month
		$activitiesPerMonth[$now->format('Y-m')] = $this->api->getDailyActivities($now->format('Y-m'));

		for ($i = $months; $i >= 0; $i--)
		{
			// Retrieve activities per month
			$monthObject = $now->subMonth($i);

			$m = $monthObject->format('Y-m');

			$startOfMonth = $monthObject->startOfMonth()->format('Y-m-d');
			$endOfMonth = $monthObject->endOfMonth()->format('Y-m-d');

			if ( $monthObject->format('Y-m') == Carbon::now()->format('Y-m') )
			{
				$endOfMonth = Carbon::now()->format('Y-m-d');
			}

			$activitiesPerMonth[$m] = $this->api->getDailyActivities([
					'from' => $startOfMonth,
					'to'   => $endOfMonth
				]);
		}

		return [
			'activitiesPerMonth' => $activitiesPerMonth,
		];
	}

	/**
	 * @return array
	 */
	public function getExplorer()
	{
		$this->api->mergeFoursquare = true;

		// Find number of months passed sinds registration
		$then = Carbon::parse($this->profile['profile']['firstDate']);
		$now = Carbon::now();
		$nowOriginal = Carbon::now()->format('Y-m');

		$months = $now->diffInMonths($then);

		$placesPerMonth = [];

		// This month
		$placesPerMonth[$now->format('Y-m')] = $this->api->getDailyPlaces($now->format('Y-m'));

		for ($i = $months; $i >= 0; $i--)
		{
			// Retrieve activities per month
			$monthObject = $now->subMonth($i);

			$m = $monthObject->format('Y-m');

			//Check if we need to check fo registration date
			if ( $m < Carbon::parse($this->profile['profile']['firstDate'])->format('Y-m'))
			{
				continue;
			}
			else
			{
				$placesPerMonth[$m] = $this->api->getDailyPlaces($m);
			}
		}

		$places = [];
		$tomorrow = Carbon::tomorrow()->format('Ymd');
		foreach ($placesPerMonth as $month => $log)
		{
			$m = Carbon::parse($month)->format('F Y');

			foreach ($log as $key => $item)
			{
				if($item['date'] === $tomorrow)
				{
					unset($placesPerMonth[$month][$key]);
					break;
				}

				if ( ! empty($item['segments']) )
				{
					$i = 0;
					foreach ($item['segments'] as $place)
					{
						if ( $place['place']['type'] !== 'foursquare' )
						{
							continue;
						}

						$places[$m]['places'][$place['place']['id']] = $place['place'];

						if ( ! empty($place['place']['foursquare']['city']) )
						{
							$i++;

							$places[$m]['cities'][$place['place']['foursquare']['city']] = $place['place']['foursquare']['city'];
						}
					}
				}
			}
		}

		return [
			'currentMonthSlug' => Carbon::now()->format('F-Y'),
			'lastLocation'     => end(end($placesPerMonth[$nowOriginal])['segments']),
			'placesPerMonth'   => $places,
		];
	}

	/**
	 * @param string $selectedMonth
	 *
	 * @return array
	 */
	public function getExplorerMonth($selectedMonth)
	{
		$this->api->mergeFoursquare = true;
		$dateObject = Carbon::parse($selectedMonth);
		$currentDateObject = Carbon::now();

		$month = $dateObject->format('Y-m');
		$currentMonth = $currentDateObject->format('Y-m');

		if ( $month > $currentMonth )
		{
			return $this->redirector->route('explorer.month', ['month' => $currentDateObject->format('F-Y')]);
		}

		$startOfMonth = $dateObject->startOfMonth()->format('Y-m-d');
		$endOfMonth = $dateObject->endOfMonth()->format('Y-m-d');

		//Check if we need to check fo registration date
		if ( $month == Carbon::parse($this->profile['profile']['firstDate'])->format('Y-m') )
		{
			//Check if start of month is not past registration date
			$registrationDate = Carbon::parse($this->profile['profile']['firstDate']);

			if ( $registrationDate->lt($dateObject) )
			{
				$startOfMonth = $registrationDate->format('Y-m-d');
			}
		}

		if ( $month == Carbon::now()->format('Y-m') )
		{
			$endOfMonth = Carbon::now()->format('Y-m-d');
		}

		$placesThisMonth = $this->api->getDailyPlaces([
				'from' => $startOfMonth,
				'to'   => $endOfMonth
			]);
		$activitiesThisMonth = $this->api->getDailyActivities([
				'from' => $startOfMonth,
				'to'   => $endOfMonth
			]);

		$places = $this->moves->organizeSegmentsPerDay($placesThisMonth);
		$travels = $this->moves->organizeTravelsPerDay($activitiesThisMonth);

		$placesArray = [];

		foreach ($placesThisMonth as $month => $item)
		{
			if ( ! empty($item['segments']) )
			{
				foreach ($item['segments'] as $place)
				{
					if ( $place['place']['type'] !== 'foursquare' )
					{
						continue;
					}

					$placesArray[] = $place;
				}
			}
		}

		$headerPlaces = [];
		$placesPerColumn = 5;
		$column = 1;
		$i = 0;
		$nPlaces = 0;

		foreach ($placesArray as $place)
		{
			$nPlaces++;

			if ( $i > 0 AND $i % $placesPerColumn == 0 )
			{
				$column++;
			}

			$headerPlaces[$column][] = $place;

			$i++;
		}

		return [
			'month'            => Carbon::parse($selectedMonth)->format('F Y'),
			'prevMonth'        => Carbon::parse($selectedMonth)->subMonth()->format('F Y'),
			'nextMonth'        => Carbon::parse($selectedMonth)->addMonth()->format('F Y'),
			'nPlaces'          => $nPlaces,
			'headerPlaces'     => $headerPlaces,
			'placesThisMonth'  => $places,
			'travelsThisMonth' => $travels,
		];
	}

    function goBackInTime($daysBack = 1)
    {
        // Today is empty, get yesterday
        $yesterday = Carbon::now()->subDays($daysBack)->format('Ymd');

        $dailyPlaces = $this->api->getDailyPlaces($yesterday);
        $dailyStoryline = $this->api->getDailyStoryLine($yesterday, ['trackPoints' => 'true']);

        if ( empty(end($dailyStoryline)['segments']) )
        {
            $this->goBackInTime($daysBack++);
        }

        return [
            $dailyPlaces,
            $dailyStoryline,
        ];
    }
}