<?php namespace App\Services;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class Moves
{
    private $httpClient;
    private $authorizeUrl   = 'https://api.moves-app.com/oauth/v1/authorize?';
    private $accessTokenUrl = 'https://api.moves-app.com/oauth/v1/access_token?';
    private $tokenInfoUrl   = 'https://api.moves-app.com/oauth/v1/tokeninfo?';
    private $redirectUrl;
    private $clientKey;
    private $clientSecret;

    public function __construct()
    {
        // Create the HttpClient
        $this->httpClient = new HttpClient();
        $this->httpClient->setDefaultOption('verify', false);

        // Set keys
        $this->clientKey    = config('services.moves.key');
        $this->clientSecret = config('services.moves.secret');
        $this->redirectUrl  = config('services.moves.redirect');
    }

    /**
     * Get the request url
     *
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->authorizeUrl . 'response_type=code&client_id=' . $this->clientKey . '&redirect_uri=' . $this->redirectUrl . '&scope=' . urlencode('activity location');
    }

    /**
     * Authenticate
     *
     * @param $requestToken
     */
    public function authenticate($requestToken)
    {
        $url = $this->accessTokenUrl;
        $query = [
            'grant_type'    => 'authorization_code',
            'code'          => $requestToken,
            'client_id'     => $this->clientKey,
            'client_secret' => $this->clientSecret,
            'redirect_uri'  => $this->redirectUrl,
        ];

        $response = $this->httpClient->post($url,[
            'query' => $query
        ])->json();

        if (!empty($response['access_token']))
        {
            $this->storeTokens($response['access_token'], $response['refresh_token'], $response['expires_in']);
        }
    }

    /**
     * Refresh access token
     *
     * @param $refreshToken
     */
    public function refreshToken($refreshToken)
    {
        $url = $this->accessTokenUrl . 'access_token';
        $query = [
            'grant_type'    => 'refresh_token',
            'refresh_token' => $refreshToken,
            'client_id'     => $this->clientKey,
            'client_secret' => $this->clientSecret,
        ];

        $response = $this->httpClient->post($url,[
            'query' => $query
        ])->json();

        if (!empty($response['access_token']))
        {
            $this->storeTokens($response['access_token'], $response['refresh_token'], $response['expires_in'])->persist();
        }
    }

    /**
     * Check if access token is still valid
     *
     * @param $token
     *
     * @return bool
     */
    public function tokenIsValid($token)
    {
        $url = $this->tokenInfoUrl . 'access_token=' . $token;
        $response = $this->httpClient->get($url);

        if($response->getStatusCode() === 200)
        {
            return true;
        }

        return false;
    }

    public function getTokens()
    {
        if(Session::has('access_token'))
        {
            $tokens = [
                'storage'       => 'session',
                'access_token'  => Session::get('access_token'),
                'refresh_token' => Session::get('refresh_token'),
                'expires_in'    => Session::get('expires_in'),
            ];

            return $tokens;
        }
        else
        {
            return json_decode(Storage::disk('local')->get('movesTokens.json'), true);
        }
    }

    /**
     * Store Token in session
     *
     * @param $accessToken
     * @param $refreshToken
     * @param $expires
     *
     * @return array
     */
    private function storeTokens($accessToken, $refreshToken, $expires)
    {
        Session::put('access_token', $accessToken);
        Session::put('refresh_token', $refreshToken);
        Session::put('expires_in', $expires);

        return $this->persist($accessToken, $refreshToken, $expires);
    }

    /**
     * Store Token to file
     *
     * @param $accessToken
     * @param $refreshToken
     * @param $expires
     *
     * @return array
     */
    private function persist($accessToken , $refreshToken, $expires)
    {
        $tokens = [
            'storage' => 'file',
            'access_token' => $accessToken,
            'refresh_token' => $refreshToken,
            'expires_in' => $expires,
        ];

        Storage::disk('local')->put('movesTokens.json', json_encode($tokens));

        return $tokens;
    }


}
