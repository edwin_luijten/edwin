<?php namespace App\Services\Cache;

use Illuminate\Cache\CacheManager;

class CacheService implements CacheInterface {

    /**
     * @var CacheManager
     */
    protected $cache;

    /**
     * @var string
     */
    protected $tag;

    /**
     * @var integer
     */
    protected $minutes;

    /**
     * @var bool
     */
    private $useTags = false;

    /**
     * Construct
     *
     * @param CacheManager $cache
     * @param string $tag
     * @param integer $minutes
     */
    public function __construct(CacheManager $cache, $tag = null, $minutes = 60)
    {
        $this->cache = $cache;
        $this->tag = $tag;
        $this->minutes = $minutes;

        if(config('cache.default') === 'redis')
        {
            $this->useTags = true;
        }
    }

    /**
     * Set the cache tag
     *
     * @param $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * Get
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        if($this->useTags)
        {
            return $this->cache->tags($this->tag)->get($key);
        }

        return $this->cache->get($key);
    }

    /**
     * Put
     *
     * @param string $key
     * @param mixed $value
     * @param integer $minutes
     * @return mixed
     */
    public function put($key, $value, $minutes = null)
    {
        if ( is_null($minutes))
        {
            $minutes = $this->minutes;
        }

        if($this->useTags)
        {
            return $this->cache->tags($this->tag)->put($key, $value, $minutes);
        }

        return $this->cache->put($key, $value, $minutes);
    }

    /**
     * Has
     *
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        if($this->useTags)
        {
            return $this->cache->tags($this->tag)->has($key);
        }

        return $this->cache->has($key);
    }
}