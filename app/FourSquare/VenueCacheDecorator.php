<?php namespace App\Foursquare;

use App\Services\Cache\CacheInterface;

class VenueCacheDecorator extends AbstractVenueDecorator {

    protected $cache;

    public function __construct(Venue $venue, CacheInterface $cache)
    {
        parent::__construct($venue);

        $this->cache = $cache;
    }

    public function get($foursquareId)
    {
        $key = md5($foursquareId);

        if($this->cache->has($key))
        {
            return $this->cache->get($key);
        }

        $venue = $this->venue->get($foursquareId);

        $this->cache->put($key, $venue);

        return $venue;
    }
}