<?php namespace App\Foursquare;

abstract class AbstractVenueDecorator {

    protected $venue;

    public function __construct(Venue $venue)
    {
        $this->venue = $venue;
    }

    public function get($foursquareId)
    {
        return $this->venue->get($foursquareId);
    }
}