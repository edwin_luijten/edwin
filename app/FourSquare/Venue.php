<?php namespace App\Foursquare;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Str;

class Venue
{

	private $httpClient;
	private $endpoint = 'https://api.foursquare.com/v2/venues/';
	private $version;
	protected $response;

	public function __construct()
	{
		$this->httpClient = new HttpClient(['base_url' => $this->endpoint]);
		$this->httpClient->setDefaultOption('verify', false);

		$tokens = [
			'client_id'     => config('services.foursquare.key'),
			'client_secret' => config('services.foursquare.secret'),
		];

		$this->clientId = $tokens['client_id'];
		$this->clientSecret = $tokens['client_secret'];
		$this->version = date('Ymd');
	}

	/**
	 * Get venue details
	 *
	 * @param $foursquareId
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function get($foursquareId)
	{
		$response = $this->httpClient->get($foursquareId, [
			'query' => [
				'client_id'     => $this->clientId,
				'client_secret' => $this->clientSecret,
				'v'             => $this->version,
			]
		]);

		$this->response = $this->handleResponse($response);

		return $this->info();
	}

	public function info()
	{
		$info = [
			'city'            => (! isset($this->response['response']['venue']['location']['city']) ? null : $this->response['response']['venue']['location']['city']),
			'lat'             => $this->response['response']['venue']['location']['lat'],
			'lon'             => $this->response['response']['venue']['location']['lng'],
			'tags'            => $this->createTagsFromCategories(),
			'primaryCategory' => $this->getPrimaryCategory(),
		];

		return $info;
	}

	/**
	 * Handles the response from the moves api
	 *
	 * @param $response
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	private function handleResponse($response)
	{
		try
		{
			$responseJsonArray = json_decode($response->getBody(), true);
		}
		catch (\RuntimeException $e)
		{
			throw new \Exception("Invalid JSON response: " . $e->getMessage());
		}

		return $responseJsonArray;
	}

	/**
	 * Create tags from venue categories
	 *
	 * @param array $categories
	 *
	 * @return array
	 */
	private function createTagsFromCategories($categories = [])
	{
		if ( empty($categories) )
		{
			$categories = $this->response['response']['venue']['categories'];
		}

		foreach ($categories as $i => $category)
		{
			$icon = $category['icon']['prefix'];
			$parts = explode('/', $icon);
			$categoryTag = end($parts);
			$categoryTagParts = explode('_', $categoryTag);

			$tags[] = $categoryTagParts[0];

			if ( ! empty($categoryTagParts[1]) )
			{
				$tags[] = $categoryTagParts[0] . '-' . $categoryTagParts[1];
			}

			$tags[] = Str::slug($category['shortName']);

			return $tags;
		}
	}

	/**
	 * Get the primary category of the venue
	 *
	 * @param array $categories
	 *
	 * @return mixed
	 */
	private function getPrimaryCategory($categories = [])
	{
		if ( empty($categories) )
		{
			$categories = $this->response['response']['venue']['categories'];
		}

		foreach ($categories as $i => $category)
		{
			$category['name'] = $category['shortName'];

			if ( array_key_exists('primary', $category) )
			{
				return $category;
			}
		}
	}
}