<?php namespace App\Moves;

abstract class AbstractApiDecorator {

    protected $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    public function getProfile()
    {
        return $this->api->getProfile();
    }

    public function getDailyPlaces(...$args)
    {
        return $this->api->getDailyPlaces($args);
    }

    public function getDailySummary(...$args)
    {
        return $this->api->getDailySummary($args);
    }

    public function getDailyActivities(...$args)
    {
        return $this->api->getDailyActivities($args);
    }
}