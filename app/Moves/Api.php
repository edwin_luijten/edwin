<?php namespace App\Moves;

use App\Services\Moves;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Api
{
    private $httpClient;
    private $endpoint = 'https://api.moves-app.com/api/1.1/';
    private $movesService;
    private $venue;
    public $mergeFoursquare = true;

    public function __construct()
    {
        $this->httpClient = new HttpClient(['base_url' => $this->endpoint]);
        $this->httpClient->setDefaultOption('verify', false);

        $this->movesService = new Moves();
        $tokens = $this->movesService->getTokens();

        $this->accessToken = $tokens['access_token'];
        $this->refreshToken = $tokens['refresh_token'];

        $this->venue = app('App\Foursquare\Venue');
    }

    /**
     * Get profile
     *
     * @return mixed
     */
    public function getProfile()
    {
        $this->mergeFoursquare = false;
        return $this->get('user/profile');
    }

    /**
     * Get a daily summary
     *
     * @return mixed
     */
    public function getDailySummary(...$args)
    {
        return $this->getRange('user/summary/daily', $args[0]);
    }

    /**
     * Get daily activities
     *
     * @return mixed
     */
    public function getDailyActivities(...$args)
    {
        $this->mergeFoursquare = false;
        return $this->getRange('user/activities/daily', $args[0]);
    }

    /**
     * Get daily places
     *
     * @return mixed
     */
    public function getDailyPlaces(...$args)
    {
        return $this->getRange('user/places/daily', $args);
    }

    /**
     * Get daily storyline
     *
     * @return mixed
     */
    public function getDailyStoryline(...$args)
    {
        return $this->getRange('user/storyline/daily', $args);
    }

    /**
     * Get the result of the api endpoint
     *
     * @param $path
     * @param $params
     *
     * @return mixed
     * @throws \Exception
     */
    public function get($path, $params = [])
    {
        try
        {
            $response = $this->httpClient->get($path, [
                'headers' => [
                    'Authorization'   => 'Bearer ' . $this->accessToken,
                    'Accept'          => 'application/json',
                    'Accept-Encoding' => 'gzip'
                ],
                'query'   => $params
            ]);
        }
        catch (RequestException $e)
        {
            die($e->getMessage());
        }
        catch (ClientException $e)
        {
            $errorCode = $e->getCode();

            if($errorCode === 401)
            {
                $this->movesService->refreshToken($this->refreshToken);
            }
        }

        if($this->mergeFoursquare)
        {
            return $this->mergeFoursquare($this->handleResponse($response));
        }

        return $this->handleResponse($response);
    }

    /**
     * Get a date range
     *
     * @param $path
     * @param $args
     *
     * @return mixed
     */
    public function getRange($path, $args)
    {
        $arg0 = isset($args[0]) ? $args[0] : false;
        $arg1 = isset($args[1]) ? $args[1] : false;

        $processArguments = new ApiArguments();

        list ( $extraPath, $params) = $processArguments->process($arg0, $arg1);
        $params = $params ?: [];

        return $this->get($path . $extraPath, $params);
    }

    /**
     * Handles the response from the moves api
     *
     * @param $response
     *
     * @return mixed
     * @throws \Exception
     */
    private function handleResponse($response)
    {
        try
        {
            $responseJsonArray = json_decode($response->getBody(), true);
        }
        catch (\RuntimeException $e)
        {
            throw new \Exception("Invalid JSON response: " . $e->getMessage());
        }

        return $responseJsonArray;
    }

    private function mergeFoursquare($responseJsonArray)
    {
        foreach($responseJsonArray as $k =>  $items)
        {
            foreach($items as $i => $item)
            {
                if(is_array($item))
                {
                    foreach($item as $s => $segment)
                    {
                        foreach($segment as $p => $place)
                        {
                            if (!empty($responseJsonArray[$k][$i][$s]['type']) AND $responseJsonArray[$k][$i][$s]['type'] === 'place' )
                            {
                                if($responseJsonArray[$k][$i][$s]['place']['type'] === 'foursquare')
                                {
                                    $responseJsonArray[$k][$i][$s]['place']['foursquare'] = $this->venue->get($responseJsonArray[$k][$i][$s]['place']['foursquareId']);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $responseJsonArray;
    }
}