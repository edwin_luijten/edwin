<?php namespace App\Moves;

use App\Services\Cache\CacheInterface;

class ApiCacheDecorator extends AbstractApiDecorator
{

	protected $cache;
	public $mergeFoursquare = true;
	public $fresh = false;

	public function __construct(Api $api, CacheInterface $cache)
	{
		parent::__construct($api);

		$this->cache = $cache;
	}

	public function getProfile()
	{
		$key = md5('profile');

		if ( $this->cache->has($key) )
		{
			return $this->cache->get($key);
		}

		$profile = $this->api->getProfile();

		$this->cache->put($key, $profile);

		return $profile;
	}

	public function getDailySummary(...$args)
	{
		$key = md5('daily_summary' . implode('.', $args[0]));

		if ( $this->cache->has($key) AND $this->fresh === false)
		{
			return $this->cache->get($key);
		}

		$dailySummary = $this->api->getDailySummary($args);

		$this->cache->put($key, $dailySummary);

		return $dailySummary;
	}

	public function getDailyStoryline(...$args)
	{
		$key = md5('daily_storyline.' . implode('.', array_dot($args)));

		if ( $this->cache->has($key) AND $this->fresh === false)
		{
			return $this->cache->get($key);
		}

		$dailyStoryline = $this->api->getDailyStoryline($args[0]);

		$this->cache->put($key, $dailyStoryline);

		return $dailyStoryline;
	}

	public function getDailyActivities(...$args)
	{
		$key = md5('daily_activities.' . implode('.', array_dot($args)));

		if ( $this->cache->has($key) AND $this->fresh === false)
		{
			return $this->cache->get($key);
		}

		$dailyActivities = $this->api->getDailyActivities($args);

		$this->cache->put($key, $dailyActivities);

		return $dailyActivities;
	}

	public function getDailyPlaces(...$args)
	{
		$key = md5('daily_places.' . implode('.', array_dot($args)));

		if ( $this->cache->has($key) AND $this->fresh === false)
		{
			return $this->cache->get($key);
		}

		$this->api->mergeFoursquare = $this->mergeFoursquare;
		$dailyPlaces = $this->api->getDailyPlaces($args);

		$this->cache->put($key, $dailyPlaces);

		return $dailyPlaces;
	}
}