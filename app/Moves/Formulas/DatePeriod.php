<?php namespace App\Moves\Formulas;

use Carbon\Carbon;

/**
 * Build API request arguments for calls like:
 *  - $Moves->dailySummary(array('from' => '2013-11-10', 'to' => '2013-11-20'));
 *  - $Moves->dailySummary(array('from' => new DateTime('2013-11-10'), 'to' => new DateTime('2013-11-20')));
 *  - $Moves->dailyActivities(array('pastDays' => 3));
 *  - $Moves->dailyStoryline(array('trackPoints' => 'true'));
 */
class DatePeriod implements FormulaInterface
{

    public function test($arg0, $arg1)
    {
        return is_array($arg0) AND false === $arg1;

    }

    public function process($arg0, $arg1)
    {
        list($extraPath, $params) = array("", $arg0);

        if (isset($params['from']) AND $params['from'] instanceof Carbon)
        {
            $params['from'] = $params['from']->format(FormulaInterface::FORMAT);
        }

        if (isset($params['to']) AND $params['to'] instanceof Carbon)
        {
            $params['to'] = $params['to']->format(FormulaInterface::FORMAT);
        }

        if (!isset($params['from']) AND !isset($params['to']) AND !isset($params['pastDays']) AND !isset($params['updatedSince']))
        {
            $extraPath = '/' . Carbon::now()->format(FormulaInterface::FORMAT);
        }

        return [ $extraPath, $params ];
    }
}