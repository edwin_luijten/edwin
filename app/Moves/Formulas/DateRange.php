<?php namespace App\Moves\Formulas;

use Carbon\Carbon;

/**
 * Build API request arguments for calls like:
 *  - $Moves->dailySummary('2013-11-10', '2013-11-20');
 *  - $Moves->dailySummary(new DateTime('2013-11-10'), new DateTime('2013-11-20'));
 */
class DateRange implements FormulaInterface
{

    public function test($arg0, $arg1)
    {
        return !is_array($arg0) AND !is_array($arg1);
    }

    public function process($arg0, $arg1)
    {

        list($extraPath, $params) = ['', ['from' => $arg0, 'to' => $arg1]];

        if (isset($params['from']) AND $params['from'] instanceof Carbon)
        {
            $params['from'] = $params['from']->format(FormulaInterface::FORMAT);
        }
        else
        {
            $params['from'] = Carbon::now()->format(FormulaInterface::FORMAT);
        }

        if (isset($params['to']) AND $params['to'] instanceof Carbon)
        {
            $params['to'] = $params['to']->format(FormulaInterface::FORMAT);
        }
        else
        {
            $params['to'] = Carbon::now()->format(FormulaInterface::FORMAT);
        }

        return [ $extraPath, $params];
    }
}