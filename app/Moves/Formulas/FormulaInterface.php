<?php namespace App\Moves\Formulas;

interface FormulaInterface {

    const FORMAT = 'Y-m-d';

    public function test($arg0, $arg1);

    public function process($arg0, $arg1);
}