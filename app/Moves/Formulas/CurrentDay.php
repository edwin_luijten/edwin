<?php namespace App\Moves\Formulas;

use Carbon\Carbon;

/**
 * Build API request arguments for calls like:
 *  - $Moves->dailyStoryline();
 */
class CurrentDay implements FormulaInterface
{

    public function test($arg0, $arg1)
    {
        return (false === $arg0 && false === $arg1);
    }

    public function process($arg0, $arg1)
    {
        list($extraPath, $params) = ['/' . Carbon::now()->format(FormulaInterface::FORMAT), false];

        return [ $extraPath, $params];
    }
}