<?php namespace App\Moves;

use Carbon\Carbon;

class Moves {

    private $endOfDay = 24.00;

    public function calculateRotation($position)
    {
        $max = 13;
        $middle =  (50 / 100) * $max;

        if(is_null($position))
        {
            return $max;
        }

        $rotation = ($position / 100) * $max;

        if($rotation < $middle)
        {
            $rotation = '-' . $rotation;
        }

        return $rotation;

    }

    public function organizeSegmentsPerDay($resultSet)
    {
        $day = [];

        foreach($resultSet as $result)
        {
            if(!empty($result['segments']))
            {
                foreach($result['segments'] as $segment)
                {
                    $monthObject = Carbon::parse($segment['startTime']);

                    $currentDay = $monthObject->format('Y-m-d');
                    $nextDay = $monthObject->addDay()->format('Y-m-d');

                    $startTime = Carbon::parse($segment['startTime']);
                    $endTime = Carbon::parse($segment['endTime']);
                    $duration =  $endTime->diffInHours($startTime);

                    list($startHours, $startMinutes) = explode(':', $startTime->toTimeString());

                    $startTimeInt = $startHours . '.' .  $startMinutes;

                    if($segment['type'] === 'unknown')
                    {
                        continue;
                    }

                    if($segment['type'] === 'place')
                    {
                        if($segment['place']['type'] === 'unknown')
                        {
                            continue;
                        }

                        $item = $this->createPlace($segment);
                    }
                    else if($segment['type'] === 'move')
                    {
                        if(!empty($segment['activities']))
                        {
                            foreach($segment['activities'] as $activity)
                            {
                                $item['activities'][] = $this->createActivity($activity);
                            }
                        }
                    }

                    $item['timeStamp'] = $startTime->format('Y-m-d H:i:s');
                    $item['start'] = $startTimeInt;
                    $item['duration'] = $duration;
                    $item['startPercentage'] = ($startTimeInt / $this->endOfDay) * 100;
                    $item['durationPercentage'] = ($duration / $this->endOfDay) * 100;

                    $cssMoment = 'brief';
                    $cssDuration = 'half-hour';

                    if($duration == 1)
                    {
                        $cssDuration = 'hour';
                    }
                    else if($duration >= 3)
                    {
                        $cssMoment = 'long';
                        $cssDuration = 'half-day';
                    }

                    $item['cssMoment'] = $cssMoment;
                    $item['cssDuration'] = $cssDuration;
                    $item['labelRotation'] = $this->calculateRotation($startTimeInt);

                    // Add item to day array
                    $day[$currentDay][$startTimeInt] = $item;

                    // Calculate duration, if a duration passed end of day, copy the item,
                    // and move the item to the next day with the remaining duration
                    $exceeded = 0;

                    if(($startTimeInt + $duration) > $this->endOfDay)
                    {
                        $exceeded = ($startTimeInt + $duration) - $this->endOfDay;
                    }

                    if($exceeded > 0)
                    {
                        $day[$currentDay][$startTimeInt]['duration'] = $duration - $exceeded;
                        $day[$currentDay][$startTimeInt]['durationPercentage'] = ($exceeded / $this->endOfDay) * 100;


                        // Copy segment to next day
                        $day[$nextDay]['00.00'] = $item;
                        $day[$nextDay]['00.00']['isRemainingFromPreviousDay'] = true;
                        $day[$nextDay]['00.00']['start'] = '00.00';
                        $day[$nextDay]['00.00']['duration'] = $exceeded;
                        $day[$nextDay]['00.00']['startPercentage'] = '00.00%';
                        $day[$nextDay]['00.00']['durationPercentage'] = ($exceeded / $this->endOfDay) * 100;
                        $day[$currentDay][$startTimeInt]['labelRotation'] = $this->calculateRotation(null);
                    }
                }
            }
        }

        foreach($day as $date => $items)
        {
            $i = 0;
            $total = count($items);

            foreach($items as $time => $item)
            {
                $i++;

                if($i === $total)
                {
                    $top = $item['startPercentage'];
                    $height = $item['durationPercentage'];

                    if(($top + $height) > 90)
                    {
                        $diff = ($top + $height) - 100;

                        $day[$date][$time]['durationPercentage'] = $item['durationPercentage'] - $diff;
                        $day[$date][$time]['fitted'] = true;
                    }
                }
            }
        }

        return $day;
    }

    public function organizeTravelsPerDay($resultSet)
    {
        $day = [];

        foreach($resultSet as $result)
        {
            if(!empty($result['segments']))
            {
                foreach($result['segments'] as $segment)
                {
                    if(!empty($segment['activities']))
                    {
                        foreach($segment['activities'] as $activity)
                        {
                            $monthObject = Carbon::parse($activity['startTime']);

                            $currentDay = $monthObject->format('Y-m-d');
                            $nextDay = $monthObject->addDay()->format('Y-m-d');

                            $startTime = Carbon::parse($activity['startTime']);
                            $endTime = Carbon::parse($activity['endTime']);
                            $duration =  $endTime->diffInMinutes($startTime) / 60;

                            list($startHours, $startMinutes) = explode(':', $startTime->toTimeString());

                            $startTimeInt = $startHours . '.' .  $startMinutes;

                            if($segment['type'] === 'unknown')
                            {
                                continue;
                            }

                            $item['type'] = $activity['activity'];
                            $item['timeStamp'] = $startTime->format('Y-m-d H:i:s');
                            $item['start'] = $startTimeInt;
                            $item['duration'] = $duration;
                            $item['startPercentage'] = ($startTimeInt / $this->endOfDay) * 100;
                            $item['durationPercentage'] = ($duration / $this->endOfDay) * 100;

                            $cssMoment = 'brief';
                            $cssDuration = 'few-minutes';

                            if($duration < 0.5 AND $duration > 0.2)
                            {
                                $cssDuration = 'half-hour';
                            }
                            else if($duration == 1)
                            {
                                $cssDuration = 'hour';
                            }
                            else if($duration >= 3)
                            {
                                $cssMoment = 'long';
                                $cssDuration = 'half-day';
                            }

                            $item['cssMoment'] = $cssMoment;
                            $item['cssDuration'] = $cssDuration;
                            $item['labelRotation'] = $this->calculateRotation($startTimeInt);

                            // Add item to day array
                            $day[$currentDay][$startTimeInt] = $item;

                            // Calculate duration, if a duration passed end of day, copy the item,
                            // and move the item to the next day with the remaining duration
                            $exceeded = 0;

                            if(($startTimeInt + $duration) > $this->endOfDay)
                            {
                                $exceeded = ($startTimeInt + $duration) - $this->endOfDay;
                            }

                            if($exceeded > 0)
                            {
                                $day[$currentDay][$startTimeInt]['duration'] = $duration - $exceeded;
                                $day[$currentDay][$startTimeInt]['durationPercentage'] = ($exceeded / $this->endOfDay) * 100;


                                // Copy segment to next day
                                $day[$nextDay]['00.00'] = $item;
                                $day[$nextDay]['00.00']['isRemainingFromPreviousDay'] = true;
                                $day[$nextDay]['00.00']['start'] = '00.00';
                                $day[$nextDay]['00.00']['duration'] = $exceeded;
                                $day[$nextDay]['00.00']['startPercentage'] = '00.00%';
                                $day[$nextDay]['00.00']['durationPercentage'] = ($exceeded / $this->endOfDay) * 100;
                                $day[$currentDay][$startTimeInt]['labelRotation'] = $this->calculateRotation(null);
                            }
                        }
                    }

                }
            }
        }

        foreach($day as $date => $items)
        {
            $i = 0;
            $total = count($items);

            foreach($items as $time => $item)
            {
                $i++;

                if($i === $total)
                {
                    $top = $item['startPercentage'];
                    $height = $item['durationPercentage'];

                    if(($top + $height) > 90)
                    {
                        $diff = ($top + $height) - 100;

                        $day[$date][$time]['durationPercentage'] = $item['durationPercentage'] - $diff;
                        $day[$date][$time]['fitted'] = true;
                    }
                }
            }
        }

        return $day;
    }

    private function createPlace($segment)
    {
        // Create item
        $item['type'] = $segment['type'];
        $item['name'] = $segment['place']['name'];

        if(!empty($segment['place']['foursquare']))
        {
            $iconUrl = str_replace('https://', '', $segment['place']['foursquare']['primaryCategory']['icon']['prefix']);
            $stringParts = explode('/', $iconUrl);

            unset($stringParts[0]);
            unset($stringParts[1]);
            unset($stringParts[2]);

            $stringParts = array_values($stringParts);

            $categorySlug = $stringParts[0];
            $prefix = $stringParts[1];
            $item['icon'] = '/foursquare/' . $categorySlug . '/' . $prefix;
            $item['category'] = $segment['place']['foursquare'];
        }

        return $item;
    }

}