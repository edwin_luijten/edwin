<?php namespace App\Moves;

use App\Moves\Formulas\CurrentDay;
use App\Moves\Formulas\DatePeriod;
use App\Moves\Formulas\DateRange;
use App\Moves\Formulas\FormulaInterface;
use App\Moves\Formulas\SingleDay;

class ApiArguments {

    protected $formulas = [];

    public function __construct()
    {
        $this->addFormula(new CurrentDay());
        $this->addFormula(new SingleDay());
        $this->addFormula(new DateRange());
        $this->addFormula(new DatePeriod());
    }

    public function process($arg0, $arg1)
    {
        foreach($this->formulas as $formula)
        {
            if($formula->test($arg0, $arg1))
            {
                return $formula->process($arg0, $arg1);
            }
        }
    }

    private function addFormula(FormulaInterface $formula)
    {
        $this->formulas[] = $formula;
    }
}