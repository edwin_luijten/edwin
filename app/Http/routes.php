<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['middleware' => ['moves.auth'], 'as' => 'index', 'uses' => 'PageController@getIndex']);
Route::get('sport', ['middleware' => ['moves.auth', 'pjax'], 'as' => 'sport', 'uses' => 'PageController@getSport']);
Route::get('explorer', ['middleware' => ['moves.auth', 'pjax'], 'as' => 'explorer', 'uses' => 'PageController@getExplorer']);
Route::get('explorer/{month}', ['middleware' => ['moves.auth', 'pjax'], 'as' => 'explorer.month', 'uses' => 'PageController@getExplorerMonth']);

Route::get('foursquare/{category}/{icon}', ['as' => 'foursquare.icon', 'uses' => 'IconController@getIcon']);

Route::group(['prefix' => 'moves'], function(){
    Route::get('callback', ['as' => 'moves.callback', 'uses' => 'MovesController@getCallback']);
});