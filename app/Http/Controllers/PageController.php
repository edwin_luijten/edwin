<?php namespace App\Http\Controllers;

use Illuminate\Routing\Redirector;

class PageController extends Controller
{

	/**
	 * @var \App\Tracker\Tracker
	 */
	private $tracker;

	/**
	 * @var \Illuminate\Routing\Redirector
	 */
	private $redirector;

	/**
	 * @param \Illuminate\Routing\Redirector $redirector
	 */
	public function __construct(Redirector $redirector)
	{
		$this->redirector = $redirector;
		$this->tracker = app('App\Tracker\Tracker');
	}

	public function getIndex()
	{
		$this->tracker->fresh = true;
		$data = $this->tracker->getSummary();

		return view('app', $data);
	}

	/**
	 * Get the sports page
	 *
	 * @return \Illuminate\View\View
	 */
	public function getSport()
	{
		$data = $this->tracker->getSport();

		return view('sport', $data);
	}

	/**
	 * Get the explorer view
	 *
	 * @return \Illuminate\View\View
	 */
	public function getExplorer()
	{
		$data = $this->tracker->getExplorer();

		return view('explorer', $data);
	}

	/**
	 * Get the month detail view
	 *
	 * @param string $selectedMonth
	 *
	 * @return \Illuminate\View\View
	 */
	public function getExplorerMonth($selectedMonth)
	{
		$data = $this->tracker->getExplorerMonth($selectedMonth);

		return view('explorer-month', $data);
	}
}