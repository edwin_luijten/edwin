<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;

class IconController extends Controller {

    public function getIcon($category, $icon)
    {
        $file = storage_path('app/foursquare/' . $category . DIRECTORY_SEPARATOR . $icon);

        if(file_exists($file))
        {
            return Response::download($file);
        }
    }
}