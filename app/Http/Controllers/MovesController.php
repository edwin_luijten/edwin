<?php namespace App\Http\Controllers;

use App\Moves\Api;
use App\Services\Moves;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class MovesController extends Controller {

    private $api;
    private $movesService;

    public function __construct()
    {
        $this->api = new Api();
        $this->movesService = new Moves();
    }

    public function getCallback()
    {
        $code = Input::get('code');

        $this->movesService->authenticate($code);

        return Redirect::to('/');
    }
}