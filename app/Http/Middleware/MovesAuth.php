<?php namespace App\Http\Middleware;

use App\Services\Moves;
use Closure;
use Illuminate\Support\Facades\Redirect;

class MovesAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $movesService = new Moves();

        $tokens = $movesService->getTokens();

        if(!empty($tokens['access_token']))
        {
            $accessToken = $tokens['access_token'];

            if(!$movesService->tokenIsValid($accessToken))
            {
                $movesService->refreshToken($accessToken);
            }
        }
        else
        {
            if(!$request->has('code'))
            {
                return Redirect::to($movesService->getRequestUrl());
            }

            $movesService->authenticate($request->get('code'));
        }

        return $next($request);
    }
}