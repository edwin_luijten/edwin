<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Symfony\Component\DomCrawler\Crawler;

class Pjax {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $responseContent = $next($request);
		$response = new Response($responseContent);

        // Only parse when not redirecting
		if($response->isRedirection())
		{
			return $responseContent;
		}

		if ( $request->server->get('HTTP_X_PJAX') )
		{
			$content = $response->getContent();

			$crawler = new Crawler($content);

			$title = $crawler->filter('title')->html();
			$html = $crawler->filter('#page')->html();

			$responseContent->setContent('<title>' . $title . '</title>' . $html);
			$responseContent->header('X-PJAX-URL', $request->getRequestUri());
		}


        return $responseContent;
    }

	/**
	 * Check if the response is a usable response class.
	 *
	 * @param mixed $response
	 *
	 * @return bool
	 */
	protected function isAResponseObject($response)
	{
		return (is_object($response) && $response instanceof Response);
	}
}
