<?php namespace App\Console\Commands\Cache;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Cache\CacheManager;
use Illuminate\Console\Command;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Routing\RouteCollection;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CacheWarmUp extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'cache:warm-up';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Warming up the cache by crawling available routes';

    private $router;
    private $urlGenerator;
    private $cacheManager;
    private $routes;


    /**
     * Create a new command instance.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @param UrlGenerator $urlGenerator
     * @return \App\Console\Commands\Cache\CacheWarmUp
     */
	public function __construct(Router $router, UrlGenerator $urlGenerator, CacheManager $cacheManager)
	{
		parent::__construct();

        $this->router = $router;
        $this->routes = $router->getRoutes();
        $this->urlGenerator = $urlGenerator;
        $this->httpClient = new Client();
        $this->cacheManager = $cacheManager->store();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->info('Clearing cache...');
        $this->cacheManager->flush();

        $routes = $this->parseRoutes();

        $this->info('Found ' . count($routes) . ' urls to cache');

        foreach ($routes as $route)
        {
            try{

                $request = $this->httpClient->createRequest($route['method'], $route['url']);
                $request->setPort(8000);

                $response = $this->httpClient->send($request);

                if($response->getStatusCode() === 200)
                {
                    $this->info('Cached: ' . $route['url']);
                }
            }
            catch(RequestException $e){
                $this->info('');
                $this->info('Error: ' . $e->getRequest());
                $this->info('Error message: ' . $e->getMessage());
            }
        }

	}

    private function parseRoutes()
    {
        $routes = [];

        foreach($this->routes as $route)
        {
            $methods = $route->methods();
            $methodKey = array_search('GET', $methods);

            if($methods[$methodKey] !== 'GET' OR empty($route->getName()))
            {
                continue;
            }

            if(!empty($route->getName()))
            {
                $routes[] = [
                    'url' => $this->urlGenerator->route($route->getName()),
                    'method' => $methods[$methodKey],
                ];
            }
        }

        return $routes;
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
