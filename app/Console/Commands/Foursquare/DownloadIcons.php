<?php namespace App\Console\Commands\Foursquare;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Cache\CacheManager;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Routing\RouteCollection;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DownloadIcons extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'foursquare:download-icons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads category icons from foursquare';

    private $httpClient;

    private $storage;

    private $foursquareFolder = 'foursquare';

    private $sizes = [
        32,
        64,
    ];

    /**
     * Create a new command instance.
     *
     * @param \Illuminate\Contracts\Filesystem\Factory $storage
     */
    public function __construct(Factory $storage)
    {
        parent::__construct();

        $this->httpClient = new Client();
        $this->httpClient->setDefaultOption('verify', false);
        $this->storage = $storage->disk('local');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $categories = json_decode(file_get_contents(__DIR__ . '../Resources/categories.json'));

        if ( ! $this->storage->exists($this->foursquareFolder) )
        {
            $this->storage->makeDirectory($this->foursquareFolder);
        }

        if ( ! empty($categories) )
        {
            $this->createCategoryFolders($categories);
        }
    }

    private function createCategoryFolders($categories)
    {

        if ( ! empty($categories) )
        {
            foreach ($categories as $category)
            {
                $icon = str_replace('https://', '', $category->icon->prefix);
                $stringParts = explode('/', $icon);

                unset($stringParts[0]);
                unset($stringParts[1]);
                unset($stringParts[2]);

                $stringParts = array_values($stringParts);

                $categorySlug = $stringParts[0];
                $prefix = $stringParts[1];

                $categoryPath = $this->foursquareFolder . DIRECTORY_SEPARATOR . $categorySlug;

                if ( ! $this->storage->exists($categoryPath) )
                {
                    $this->storage->makeDirectory($categoryPath);
                }

                // Get icon and store
                if ( ! empty($category->icon) )
                {
                    $suffix = $category->icon->suffix;

                    foreach ($this->sizes as $size)
                    {
                        try
                        {
                            $response = $this->httpClient->get($category->icon->prefix . $size . $suffix);

                            if ( $response->getBody() )
                            {
                                $iconData = $response->getBody();
                                $filename = $prefix . $size . $suffix;

                                $this->storage->put($categoryPath . DIRECTORY_SEPARATOR . $filename, $iconData);
                            }
                        }
                        catch (ConnectException $e)
                        {
                            $this->error('Could not connect to: ' . $prefix . $size . $suffix);
                            break;
                        }
                    }
                }

                if ( ! empty($category->categories) )
                {
                    $this->createCategoryFolders($category->categories);
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
