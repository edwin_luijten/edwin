<?php namespace App\Providers;

use App\Foursquare\Venue;
use App\Foursquare\VenueCacheDecorator;
use App\Moves\Api;
use App\Moves\ApiCacheDecorator;
use App\Services\Cache\CacheService;

use App\Tracker\Tracker;
use App\Tracker\TrackerCacheDecorator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('App\Moves\Api', function ($app)
		{
			$api = new Api();

			return new ApiCacheDecorator($api, new CacheService($app['cache']), 'moves');
		});

		$this->app->bind('App\Foursquare\Venue', function ($app)
		{
			$venue = new Venue();

			return new VenueCacheDecorator($venue, new CacheService($app['cache']), 'venue');
		});

		$this->app->bind('App\Tracker\Tracker', function ($app)
		{
			$tracker = new Tracker();

			return new TrackerCacheDecorator($tracker, new CacheService($app['cache'], 'tracker'));
		});
	}
}
