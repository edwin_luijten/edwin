<?php

if(!function_exists('speedComparison'))
{
    function speedComparison($type, $speed)
    {
        if($type === 'walking')
        {
            if ( $speed <= 2 )
            {
                return 'snail';
            }
            else if ( $speed <= 4)
            {
                return 'slow';
            }
            else if ($speed <= 6)
            {
                return 'medium';
            }
            else if ($speed <= 7)
            {
                return 'ok';
            }
            else if ($speed > 7)
            {
                return 'fast';
            }
        }
        else if($type === 'running')
        {

        }
        else if($type === 'cycling')
        {
            if($speed <= 5)
            {
                return 'snail';
            }
            else if ($speed <= 15)
            {
                return 'slow';
            }
            else if ($speed <= 22)
            {
                return 'medium';
            }
            else if ($speed <= 25)
            {
                return 'ok';
            }
            else if ($speed > 25)
            {
                return 'fast';
            }
        }

    }
}

if(!function_exists('percentageOfDay'))
{
    function percentageOfDay($hours)
    {
        $minutesInDay = 14400;

        $minutes = 0;
        if (strpos($hours, ':') !== false)
        {
            // Split hours and minutes.
            list($hours, $minutes) = explode(':', $hours);
        }

        return ($hours / 24 + $minutes / $minutesInDay) * 100;
    }
}

if(!function_exists('fitInTimeline'))
{
    function fitInTimeLine($startTime, $duration)
    {
        $start = 0000;
        $mid = 1200;
        $end = 2400;
        $exceeded = 0;

        if(strlen($duration) === 1)
        {
           $duration = $duration . '000';
        }
        else if ( strlen($duration) === 2 )
        {
            $duration = $duration . '00';
        }
        else if (strlen($duration) === 3)
        {
            $duration = $duration . '0';
        }

        list($startHours, $startMinutes) = explode(':', $startTime);

        $startTimeInt = $startHours . $startMinutes;

        // If the duration passes end of day
        // We will fit the event inside the time till midnight
        // Thus, remove exceeded time
        if(($startTimeInt + $duration) > $end)
        {
            $exceeded = ($startTimeInt + $duration) - $end;
            $duration = $duration - $exceeded;
        }

        $attributes = [
            'start' => ($startTimeInt / $end) * 100,
            'duration' => ($duration / $end) * 100,
            'toNextDay' => $exceeded,
        ];

        return $attributes;
    }
}